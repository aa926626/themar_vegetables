<?php


use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\SiteSetting;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File ;
use Intervention\Image\Facades\Image;



function fixPhone($string = null)
{
    if (!$string) {
        return null;
    }

    $result = convert2english($string);
    $result = ltrim($result, '00');
    $result = ltrim($result, '0');
    $result = ltrim($result, '+');
    return $result;
}
function convert2english($string)
{
    $newNumbers = range(0, 9);
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    $string = str_replace($arabic, $newNumbers, $string);
    return $string;
}
function deleteFile($file_name, $directory = 'unknown'): void
{
    if ($file_name && $file_name != 'default.png' && file_exists("storage/images/$directory/$file_name")) {
        unlink("storage/images/$directory/$file_name");
    }
}
function uploadAllTyps($file, $directory, $width = null, $height = null)
{
    if (!File::isDirectory('storage/images/' . $directory)) {
        File::makeDirectory('storage/images/' . $directory, 0777, true, true);
    }

    $fileMimeType = $file->getClientmimeType();
    $imageCheck = explode('/', $fileMimeType);

    if ($imageCheck[0] == 'image') {
        $allowedImagesMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!in_array($fileMimeType, $allowedImagesMimeTypes))
            return 'default.png';

        return uploadeImage($file, $directory, $width, $height);
    }

    $allowedMimeTypes = ['application/pdf', 'application/msword', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if (!in_array($fileMimeType, $allowedMimeTypes))
        return 'default.png';
    return uploadFile($file, $directory);
}
function uploadFile($file, $directory)
{
    $filename = time() . rand(1000000, 9999999) . '.' . $file->getClientOriginalExtension();
    $path = base_path() . '/public/storage/images/' . $directory;
    $file->move($path, $filename);
    return $filename;
}
function uploadeImage($file, $directory, $width = null, $height = null )
{
    $img = Image::make($file)->orientate();
    $thumbsPath = 'storage/images/' . $directory;
    $name = time() . '_' .   rand(1111, 9999) . '.' . $file->getClientOriginalExtension();

    if (null != $width && null != $height)
        $img->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

    $img->save($thumbsPath . '/' . $name);
    return (string)$name;
}

function getImage($name, $directory) {
    return asset("storage/images/$directory/" . $name);
}
function defaultImage($directory) {
    return asset("/storage/images/$directory/default.png");
}
function lang(){
    return App() -> getLocale();
}
if (!function_exists('languages')) {
    function languages() {
        return ['ar', 'en'];
    }
}
if (!function_exists('defaultLang')) {
    function defaultLang() {
        return 'ar';
    }
}
function getNumbers($data , $user)
{

    $cart = Cart::where('user_id',$user->id)->first();
    $cartItems = CartItem::where('cart_id',$cart->id)->get();
    $coupon = Coupon::where('coupon_num',$data['coupon_num'])->first() ?? 0;
    $total_products = 0;
    if ($cartItems->count() > 0) {
        foreach ($cartItems as $cartItem) {
            $total_products += $cartItem->product->price * $cartItem->quantity;

        }
    }
    $total_coupon = 0;
    if ($coupon->type == 'ratio') {
        $total_coupon = $total_products - ($total_products * $coupon->discount / 100);
    }else {
        $total_coupon = $total_products - $coupon->discount;
    }


    return collect([

        'total_products' => $total_products,
        'discount_code' => $coupon->coupon_num,
        'discount'          => $coupon->discount,
        'total' => $total_coupon ,
        'coupon' => $coupon ,
        'cartItems' => $cartItems,
        'cart' => $cart,
    ]);
}
function numberFormat($number){
    return number_format((float)$number, 2, '.', '');

}
function getDistanceHaving($result, $latitude =null, $longitude = null){

    $range = 5;

    if($latitude && $longitude){
        $raw   = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') ) *cos( radians( lat ) )
           * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') )
           * sin( radians( lat ) ) ) )  AS distance');
        $results = $result->select('*', $raw)->addSelect($raw)->orderBy('distance' ,'asc')->having('distance', '<=', $range)->get();
    }else{
        $results = $result->get();
    }
    return $results;
}

function getDistance($result, $latitude, $longitude){

    $raw   = DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') ) *cos( radians( lat ) )
           * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') )
           * sin( radians( lat ) ) ) )  AS distance');

    $results = $result->select('*', $raw)->addSelect($raw)->first();
    return $results;
}

function appInformations(){
    $result = SiteSetting::pluck('value', 'key');
    return $result;
}

function setting($key = false)
{
    if ($key != false) {
        if ($s = SiteSetting::where('key', $key)->first()) {
            return $s->value;
        }
        return false;
    }
    return false;
}



