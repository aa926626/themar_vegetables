<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Traits\Firebase;

class OrderNotification extends Notification
{
    use Queueable , Firebase;
    protected $receiver, $data;

    public function __construct($order, $receiver,$type)
    {

        $this->receiver = $receiver;
        $this->data     = [
            'order_id'    => $order->id,
            'order_status'=> $order->status,
            'type'        => $type ,
        ];
    }
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toArray()
    {
        $this->data['title'] = $this->getTitle($this->data['type'] ,  $this->receiver->lang  );
        $this->sendFcmNotification( $this->data ,  $this->receiver->lang  ) ;
        return $this->data;
    }
}

