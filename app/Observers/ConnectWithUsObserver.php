<?php

namespace App\Observers;

use App\Models\ConnectWithUs;

class ConnectWithUsObserver
{
    public function saved(ConnectWithUs $ConnectWithUs)
    {

        if ($ConnectWithUs->active == 1) {
            ConnectWithUs::where('id', '!=', $ConnectWithUs->id)->update(['active' => 0]);
        }
    }
    public function created(ConnectWithUs $connectWithUs)
    {
        //
    }

    /**
     * Handle the ConnectWithUs "updated" event.
     *
     * @param  \App\Models\ConnectWithUs  $connectWithUs
     * @return void
     */
    public function updated(ConnectWithUs $connectWithUs)
    {
        //
    }

    /**
     * Handle the ConnectWithUs "deleted" event.
     *
     * @param  \App\Models\ConnectWithUs  $connectWithUs
     * @return void
     */
    public function deleted(ConnectWithUs $connectWithUs)
    {
        //
    }
    public function Deleting(ConnectWithUs $connectWithUs)
    {
        if ($connectWithUs->active == 1) {
            if (connectWithUs::where('id', '!=', $connectWithUs->id)->first()) {
                $firstConnectWithUs = connectWithUs::where('id', '!=', $connectWithUs->id)->first();
                $firstConnectWithUs->update(['active' => 1]);
            }
        }
    }
    /**
     * Handle the ConnectWithUs "restored" event.
     *
     * @param  \App\Models\ConnectWithUs  $connectWithUs
     * @return void
     */
    public function restored(ConnectWithUs $connectWithUs)
    {
        //
    }

    /**
     * Handle the ConnectWithUs "force deleted" event.
     *
     * @param  \App\Models\ConnectWithUs  $connectWithUs
     * @return void
     */
    public function forceDeleted(ConnectWithUs $connectWithUs)
    {
        //
    }
}
