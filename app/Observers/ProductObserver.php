<?php

namespace App\Observers;

use App\Models\Product;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;


class ProductObserver
{

    public function created(Product $product)
    {
        //

    }


    public function updated(Product $product)
    {
        //
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        if ($product->media()->count()) {
            foreach ($product->media as $media) {
                $path = base_path() . '/public/storage/images/products/' . $media->file_name;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }
        }
        $product->media()->delete();




    }

    public function saved(Product $product)
    {


        if (request()->has('images') && count(request()->images)) {
            $i = 1;
            foreach (request()->images as $image) {
                $file_name = $product->slug . '_' . time() . '_' . $i . '.' . $image->getClientOriginalExtension();
                $file_size = $image->getSize();
                $file_type = $image->getMimeType();
                $file_path = 'products';
                $path = base_path() . '/public/storage/images/products/' . $file_name ;


                Image::make($image->getRealPath())->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path, 100);

                $product->media()->create([
                    'file_name' => $file_name,
                    'file_size' => $file_size,
                    'file_type' => $file_type,
                    'file_status' => true,
                    'file_path' => $file_path,
                    'file_sort' => $i,
                ]);
                $i++;
            }
        }
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
