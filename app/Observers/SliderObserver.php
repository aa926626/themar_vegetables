<?php

namespace App\Observers;

use App\Models\Slider;

class SliderObserver
{
    /**
     * Handle the Slider "created" event.
     *
     * @param  \App\Models\Slider  $slider
     * @return void
     */
    public function created(Slider $slider)
    {
        //
    }

    /**
     * Handle the Slider "updated" event.
     *
     * @param  \App\Models\Slider  $slider
     * @return void
     */
    public function updated(Slider $slider)
    {
        //
    }

    public function saved(Slider $slider)
    {
        if ($slider->status == 1) {
            Slider::where('id', '!=', $slider->id)->update(['status' => 2]);
        }
    }
    public function deleted(Slider $slider)
    {
        //
    }

    public function Deleting(Slider $slider)
    {
        if ($slider->status == 1) {
            if (Slider::where('id', '!=', $slider->id)->first()) {
                $firstSlider = Slider::where('id', '!=', $slider->id)->first();
                $firstSlider->update(['status' => 1]);
            }
        }
    }
    /**
     * Handle the Slider "restored" event.
     *
     * @param  \App\Models\Slider  $slider
     * @return void
     */
    public function restored(Slider $slider)
    {
        //
    }

    /**
     * Handle the Slider "force deleted" event.
     *
     * @param  \App\Models\Slider  $slider
     * @return void
     */
    public function forceDeleted(Slider $slider)
    {
        //
    }
}
