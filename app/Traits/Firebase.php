<?php

namespace App\Traits;
use FCM;
use App\Models\UserToken ;
use App\Models\SiteSetting;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Illuminate\Http\Exceptions\HttpResponseException;

trait  Firebase
{
    use NotificationMessageTrait ;

    public function sendFcmNotification( $data , $lang = 'ar')
    {
        $data = [
            "notification" => [
                "title"    => $this->getTitle($data['type'] ,  $lang  ),
                "body"     => $this->getBody($data ,  $lang  ) ,
                'sound'    => true,
            ],
            'data'  => $data
        ];
    }
}

