<?php

namespace App\Traits;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;

trait OrderPrice {

    public function orderPrice($data , $user)
    {
        $cart = Cart::where('user_id',$user->id)->first();
        $cartItems = CartItem::where('cart_id',$cart->id)->get();
        $coupon = Coupon::where('coupon_num',$data['coupon_num'])->first();
        // total price
        $total_price = 0;
        foreach ($cartItems as $cartItem){
            $total_price += $cartItem->product->price * $cartItem->quantity;
        }
        $total_coupon = 0;
        if ($coupon->type == 'ratio') {
            $total_coupon = $total_price - ($total_price * $coupon->discount / 100);
        } else {
            $total_coupon = $total_price - $coupon->discount;
        }
        return [
            'total_price' => $total_price,
            'total_coupon' => $total_coupon,
            'coupon' => $coupon,
            'cartItems' => $cartItems,
        ];
    }


}
