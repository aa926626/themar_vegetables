<?php
namespace App\Services;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Traits\ResponseTrait;
use Illuminate\Support\Arr;
use App\Models\Order;
use App\Models\Cart;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Notification;

use App\Notifications\OrderNotification;

class OrderService {


use ResponseTrait;


    public function createOrder($data , $user)
    {
        getNumbers($data  , $user);
        if (getNumbers($data , $user) ->get('coupon')->expire_date < now()) {return $this->failMsg('الكوبون منتهي الصلاحية');}
        if (getNumbers($data , $user) ->get('coupon')->max_use == 0) {return $this->failMsg('الكوبون غير متوفر');}
        if (getNumbers($data , $user) ->get('coupon')->status != 'available') {return $this->failMsg('الكوبون غير مفعل');}
        if (getNumbers($data , $user) ->get('coupon')->use_times == getNumbers($data , $user) ->get('coupon')->max_use) {return $this->failMsg('الكوبون غير متوفر');}
        if (getNumbers($data  , $user)->get('cartItems')->count() != 0) {
               $order = Order::create([
                   'order_num'         => 'ORD-' . Str::random(15),
                   'user_id'           => $user->id,
                   'user_address_id'   => $data['user_address_id'],
                   'status'            => 0,
                   'total_products'    => getNumbers($data , $user)->get('total_products'),
                   'discount_code'     => getNumbers($data , $user)->get('discount_code') ?? null,
                   'discount'          => getNumbers($data , $user)->get('discount') ?? 0,
                   'vat_amount'        => 0,
                   'total'             => getNumbers($data , $user)->get('total'),
                   'pay_status'        => 0,
                   'pay_type'        =>  $data['pay_type'],
                   'notes'             => $data['notes'],
                   'date'              => Carbon::now()->format('Y-m-d'),
                   'time'              => Carbon::now()->format('H:i:s'),
               ]);
           }else{
               return $this->failMsg('لا يوجد منتجات في السلة');
           }

        Notification::send($order->user, new OrderNotification($order,$order->user,'NEW_ORDER'));

        foreach (getNumbers($data , $user)->get('cartItems') as $cartItem) {
            OrderProduct::create([
                'order_id'      => $order->id,
                'product_id'    => $cartItem->product_id,
                'quantity'      => $cartItem->quantity,
                'price'         => $cartItem->product->price,
                'total'         => $cartItem->product->price * $cartItem->quantity,
            ]);
        }
        CartItem::where('cart_id', getNumbers($data , $user)->get('cart')->id)->delete();
        getNumbers($data , $user)->get('coupon')->increment('use_times');


        return $order;



    }

    static function delegateOrderDetails($order)
    {


        $data['id']                 = $order->id;
        $data['user_id']            = $order->user_id;
        $data['name']               = $order->user->name;
        $data['avatar']             = $order->user->image;
        $data['phone']              = $order->user->phone;
        $data['date_time']          = $order->date .' - '. Carbon::parse($order->time)->format('h:i A');

        $data['status_text']        = $order->statusText;
        $data['preparation_status'] = trans('order.preparation_' . $order->preparation_status);

        $data['lat']                = $order->userAddress->lat;
        $data['lng']                = $order->userAddress->lng;
        $data['address']            = $order->userAddress->map_desc;

        $data['total_products']     = numberFormat($order->total_products);
        $data['deliver_price']      = numberFormat($order->deliver_price);
        $data['total']              = numberFormat($order->total) + numberFormat($order->deliver_price);

        $data['pay_type']           = $order->PayTypeText;

        return $data;
    }






}
