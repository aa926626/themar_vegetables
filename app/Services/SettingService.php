<?php
namespace  App\Services;

class SettingService {

   public static function appInformations($app_info)
    {
       $data                        = [
           'is_production'              =>$app_info['is_production'],
           'name_ar'                    =>$app_info['name_ar'],
           'name_en'                    =>$app_info['name_en'],
           'email'                      =>$app_info['email'],
           'phone'                      =>$app_info['phone'],
           'whatsapp'                   =>$app_info['whatsapp'],
           'terms_ar'                   =>$app_info['terms_ar'],
           'terms_en'                   =>$app_info['terms_en'],
           'about_ar'                   =>$app_info['about_ar'],
           'about_en'                   =>$app_info['about_en'],
           'logo'                       => (url('public/storage/images/settings/').'/'. $app_info['logo']),
           'fav_icon'                   => (url('public/storage/images/settings/'). '/'.$app_info['fav_icon']),
           'no_data_icon'               => $app_info['no_data_icon'],
           'default_user'               => (url('public/storage/images/users/'). '/'.$app_info['default_user']),
           'login_background'           => (url('public/storage/images/settings/'). '/'.$app_info['login_background']),
           'intro_logo'                 => (url('public/storage/images/settings/'). '/'.$app_info['intro_logo']),
           'intro_loader'               => (url('public/storage/images/settings/'). '/'.$app_info['intro_loader']),
           'intro_name'                 =>$app_info['intro_name_'.lang()],
           'intro_name_ar'              =>$app_info['intro_name_ar'],
           'intro_name_en'              =>$app_info['intro_name_en'],
           'intro_about'                =>$app_info['intro_about_'.lang()],
           'intro_about_ar'             =>$app_info['intro_about_ar'],
           'intro_about_en'             =>$app_info['intro_about_en'],
           'privacy_ar'                 =>$app_info['privacy_ar'],
           'privacy_en'                 =>$app_info['privacy_en'],
           'about_image_2'              =>(url('public/storage/images/settings/'). '/'.$app_info['about_image_2']),
           'about_image_1'              =>(url('public/storage/images/settings/'). '/'.$app_info['about_image_1']),
           'services_text_ar'           =>$app_info['services_text_ar'],
           'services_text_en'           =>$app_info['services_text_en'],
           'services_text'              =>$app_info['services_text_'.lang()],
           'how_work_text_ar'           =>$app_info['how_work_text_ar'],
           'how_work_text_en'           =>$app_info['how_work_text_en'],
           'how_work_text'              =>$app_info['how_work_text_'.lang()],
           'fqs_text_ar'                =>$app_info['fqs_text_ar'],
           'fqs_text_en'                =>$app_info['fqs_text_en'],
           'fqs_text'                   =>$app_info['fqs_text_'.lang()],
           'parteners_text_ar'          =>$app_info['parteners_text_ar'],
           'parteners_text_en'          =>$app_info['parteners_text_en'],
           'parteners_text'             =>$app_info['parteners_text_'.lang()],
           'contact_text_ar'            =>$app_info['contact_text_ar'],
           'contact_text_en'            =>$app_info['contact_text_en'],
           'contact_text'               =>$app_info['contact_text_'.lang()],
           'intro_email'                =>$app_info['intro_email'],
           'intro_phone'                =>$app_info['intro_phone'],
           'intro_address'              =>$app_info['intro_address'],
           'intro_meta_description'     =>$app_info['intro_meta_description'],
           'intro_meta_keywords'        =>$app_info['intro_meta_keywords'],



        ];
        return $data;
    }



}
