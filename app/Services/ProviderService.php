<?php
namespace App\Services;
use App\Models\ProductServicePrice;
use App\Models\Product;
use App\Models\Review;
use App\Models\User;

class ProviderService{

    /**  function provider Details . */
    static function providerDetails($user)
    {

        $categories = Product::where(['user_id'=>$user->id])->with('category')->get()->unique('category_id');

       $data_product = [];
       foreach ($categories as $category){

           $data_product [] = [
               'id'   => $category->category_id,
               'name' => $category->category->name,
               'subcategories' => self::providerSubcategories($category->category_id,$user->id),
           ];
       }

       return $data_product;

    }

    /**  public function provider Subcategories . */
    static function providerSubcategories($category ,$user)
    {
       $subcategories = Product::where(['user_id'=>$user,'category_id'=>$category])->get();
       $data = [];
       foreach ($subcategories as $subcategory){
           $data [] = [
               'id'    => $subcategory->id,
               'name'  => $subcategory->subcategory->name,
               'image' => $subcategory->subcategory->image,
           ];
       }
       return $data;
    }
    /** function product services prices . */

    //getProvider

}
