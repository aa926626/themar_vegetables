<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;
    protected $table   = 'cart_items';
    protected $fillable = ['product_id', 'cart_id', 'quantity'];


    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function scopeCountItems($query)
    {
        return $query->count();
    }

    public function scopeActive($query)
    {

        return $this->quantity > $this->product->quantity;


    }

}
