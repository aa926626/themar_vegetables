<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = ['user_id' , 'key'];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function CartItem()
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }
    //subtotal
    public function getSubtotalAttribute()
    {
        return $this->CartItem->sum(function ($cartItem) {
            return $cartItem->price * $cartItem->quantity;
        });
    }

    // boot
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->key = md5(uniqid(rand(), true));
        });

    }
}
