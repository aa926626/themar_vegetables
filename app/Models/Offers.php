<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offers extends BaseModel
{
    use HasFactory;
    protected $table = 'offers';
    const IMAGEPATH = 'Offers';


    protected $fillable = [
        'type',
        'max_use',
        'image',
        'use_times',
        'amount',
        'max_discount',
        'amount_type',
        'can_use_with_coupon',
        'start_date',
        'expire_date',
        'status',
    ];

    protected $casts = [
        'use_times' => 'integer',
        'used_times' => 'integer',
        'amount' => 'integer',
        'amount_type' => 'boolean',
        'start_date' => 'datetime',
        'expire_date' => 'datetime',
        'greater_than' => 'decimal',
        'status' => 'boolean',
    ];

    public function getAmountTypeAttribute($value)
    {
        return $value ? 'ratio' : 'number';
    }


    public function setAmountTypeAttribute($value)
    {
        $this->attributes['amount_type'] = $value === 'ratio' ? 1 : 0;
    }

//    public function getStartDateAttribute($value)
//    {
//        return $value ? $value->format('Y-m-d') : null;
//    }

    public function type()
    {
        return $this->amount_type == 'number' ? 'LE' : '%' ;
    }

}
