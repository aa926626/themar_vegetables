<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class FAQs extends Model
{
    use HasTranslations;
    public $translatable = ['question','answer'];
    protected $fillable = ['question','answer'];
    protected $table = 'f_a_qs';
}
