<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delegate extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function delegate()
    {
        return $this->belongsTo(User::class);
    }

    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'], 'users') : '';
            $this->attributes['image'] = uploadAllTyps($value, 'users');
        }
    }

    public function setPhotoDrivingLicenseAttribute($value)

    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['photo_driving_license']) ? deleteFile($this->attributes['photo_driving_license'], 'delegates') : '';
            $this->attributes['photo_driving_license'] = uploadAllTyps($value, 'delegates');
        }
    }

    public function setPhotoCarApplicationAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['photo_car_application']) ? deleteFile($this->attributes['photo_car_application'], 'delegates') : '';
            $this->attributes['photo_car_application'] = uploadAllTyps($value, 'delegates');
        }
    }

    public function setPhotoCarInsuranceAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['photo_car_insurance']) ? deleteFile($this->attributes['photo_car_insurance'], 'delegates') : '';
            $this->attributes['photo_car_insurance'] = uploadAllTyps($value, 'delegates');
        }
    }

    public function setFromForwardAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['from_forward']) ? deleteFile($this->attributes['from_forward'], 'delegates') : '';
            $this->attributes['from_forward'] = uploadAllTyps($value, 'delegates');
        }
    }

    public function setFromBehindAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['from_behind']) ? deleteFile($this->attributes['from_behind'], 'delegates') : '';
            $this->attributes['from_behind'] = uploadAllTyps($value, 'delegates');
        }
    }

    public function getPhotoDrivingLicenseAttribute($value)
    {
        return getImage($value, 'delegates');
    }

    public function getPhotoCarApplicationAttribute($value)
    {
        return getImage($value, 'delegates');
    }

    public function getPhotoCarInsuranceAttribute($value)
    {
        return getImage($value, 'delegates');
    }

    public function getFromForwardAttribute($value)
    {
        return getImage($value, 'delegates');
    }

    public function getFromBehindAttribute($value)
    {
        return getImage($value, 'delegates');
    }

}
