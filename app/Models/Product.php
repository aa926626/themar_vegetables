<?php

namespace App\Models;

use App\Observers\ProductObserver;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Spatie\Translatable\HasTranslations;

class Product extends BaseModel
{
    use HasFactory;
    use HasTranslations;
    use Sluggable;

    const IMAGEPATH = 'products';
  public $translatable = ['name', 'description'];
    protected $fillable = ['name', 'slug', 'description', 'price', 'quantity', 'category_id', 'featured', 'status'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function scopeFeatured($query)
    {
        return $query->whereFeatured(true);
    }

    public function scopeActive($query)
    {
        return $query->whereStatus(true);
    }

    public function scopeHasQuantity($query)
    {
        return $query->where('quantity', '>', 0);
    }

    public function scopeActiveCategory($query)
    {
        return $query->whereHas('category', function ($query) {
            $query->whereStatus(1);
        });
    }

    public function firstMedia(): MorphOne
    {
        return $this->morphOne(Media::class, 'mediable')->orderBy('file_sort', 'asc');
    }

    public function media(): MorphMany
    {
        return $this->MorphMany(Media::class, 'mediable');
    }

    public function reviews()
    {
        return $this->hasMany(ReviewRating::class);
    }

    public function OffersProducts()
    {
        return $this->hasMany(OfferProducts::class);
    }

    public function offers()
    {
        return $this->belongsToMany(Offers::class, 'offer_products');
    }

    public function subTotal($total)
    {
        return $this->amount_type == '0' ? $this->amount   : (($total * $this->amount) / 100) ;

    }

    protected static function booted()
    {
       parent::boot();
       Product::observe(ProductObserver::class);
    }


}

