<?php

namespace App\Models;

use App\Observers\ProductObserver;
use App\Observers\SliderObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Slider extends BaseModel
{
    use HasTranslations;
    use HasFactory;

    const IMAGEPATH = 'sliders';
    protected $guarded = ['id'];
    public $translatable = ['title' , 'description'];

    public function scopeActive($query)
    {
        //type 1
        return $query->whereType(1);
    }

    protected static function booted()
    {
        parent::boot();
        Slider::observe(SliderObserver::class);
    }


}
