<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferProducts extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function offer(){

        return $this->belongsTo(Offers::class);
    }

    public function product(){

        return $this->belongsTo(Product::class);
    }


}
