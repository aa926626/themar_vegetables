<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Order extends Model
{
    use HasFactory;

    const TYPE_BY_USER     = 0;


    #USER
    const STATUS_NEW       = 0;
    const STATUS_CANCEL    = 1;
    const STATUS_COMPLETED = 13;


    const PAY_TYPE_UNDEFINED = 0;
    const PAY_TYPE_CASH      = 1;
    const PAY_TYPE_WALLET    = 2;
    const PAY_TYPE_BANK      = 3;
    const PAY_TYPE_ONLINE    = 4;

    const PAY_STATUS_PENDING     = 0;
    const PAY_STATUS_DOWNPAYMENT = 1;
    const PAY_STATUS_DONE        = 2;
    const PAY_STATUS_RETURNED    = 3;


    const PREPARATION_STATUS_PENDING = 'pending';
    const PREPARATION_STATUS_DONE = 'done';
    const PREPARATION_STATUS_CANCELED = 'canceled';



    protected $fillable = [
        'order_num',
        'type',
        'user_id',
        'user_address_id',
        'delegate_id',

        'total_products',
        'deliver_price',
        'discount_code',
        'discount',
        'vat_amount',
        'total',

        'preparation_status',
        'status',
        'pay_type',
        'pay_status',
        'notes',
        'date',
    ];

    public function getTypeTextAttribute() {
        return trans('order.types_' . $this->type);
    }
    public function getStatusTextAttribute() {

        return trans('order.status_' . $this->status);
    }

    public function getPayTypeTextAttribute() {
        return trans('order.pay_type_' . $this->pay_type);
    }
    public function getPayStatusTextAttribute() {
        return trans('order.pay_status_' . $this->pay_status);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function delegate() {
        return $this->belongsTo(User::class,'delegate_id');
    }
    public function userAddress() {
        return $this->belongsTo(UserAddress::class);
    }
    public function products() {
        return $this->belongsToMany(Product::class, )->withPivot('quantity', 'price');
    }
    public function coupon() {
        return $this->belongsTo(Coupon::class);
    }
}
