<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $guarded = [];


    //type
    public function getTypeAttribute($value)
    {
        return $this->attributes['type'] = $value == 'ratio' ? '%' : '';
    }
    public function scopeHasQuantity($query)
    {
        return $query->where('use_times', '>=', 'max_use');
    }
    public function scopeStatus($query)
    {
        return $query->where('status', 'available');
    }
    public function scopeExpired($query)
    {
        return $query->where('expired_at', '>=', now());
    }


}
