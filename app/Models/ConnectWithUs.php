<?php

namespace App\Models;

use App\Observers\ConnectWithUsObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConnectWithUs extends Model
{
    use HasFactory;

 protected $guarded = ['id'];

    public function scopeActive($query)
    {
    return $query->where('active', 1)->first();
    }

    protected static function booted()
    {
        parent::boot();
        ConnectWithUs::observe(ConnectWithUsObserver::class);
    }


}
