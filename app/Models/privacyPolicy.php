<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class privacyPolicy extends Model
{
    use HasTranslations;
    public $translatable = ['title', 'description'];
    protected $fillable = ['title', 'description'];
    protected $table = 'privacy_policies';


}
