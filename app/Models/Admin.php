<?php

namespace App\Models;

use App\Http\Resources\Api\Admin\Admin\AdminResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes , HasRoles;

    protected $fillable = [
        'name',
        'phone',
        'email',
        'password',
        'avatar',
        'role_id',
        'is_notify',
        'is_blocked',
    ];
    protected $hidden = [
        'password',
    ];
    protected $casts = [
        'is_notify'  => 'boolean',
        'is_blocked' => 'boolean',
    ];

    public function getAvatarAttribute() {
        if ($this->attributes['avatar']) {
            $image =  getImage($this->attributes['avatar'], 'admins');
        } else {
            $image =  defaultImage('admins');
        }
        return $image;
    }

    public function setAvatarAttribute($value) {
        if (null != $value && is_file($value) ) {
            isset($this->attributes['avatar']) ? deleteFile($this->attributes['avatar'] , 'admins') : '';
            $this->attributes['avatar'] = uploadAllTyps($value, 'admins');
        }
    }

    public function setPasswordAttribute($value) {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
    public function role() {
        return $this->belongsTo(Role::class)->withTrashed();
    }

    public function login()
    {

        $token = Auth::guard('admin')->login($this);
        return AdminResource::make($this)->setToken($token);
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function boot() {
        parent::boot();
        /* creating, created, updating, updated, deleting, deleted, forceDeleted, restored */

   /*     static::deleted(function($model) {
            $model->deleteFile($model->attributes['avatar'], 'admins');
        });*/

    }





}
