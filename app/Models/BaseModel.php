<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function getImageAttribute() {
        if ($this->attributes['image']) {
            $image = getImage($this->attributes['image'], static::IMAGEPATH);
        } else {
            $image = defaultImage( static::IMAGEPATH);
        }
        return $image;
    }

    public function setImageAttribute($value) {
        if (null != $value && is_file($value) ) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'] , static::IMAGEPATH) : '';
            $this->attributes['image'] = uploadAllTyps($value, static::IMAGEPATH);
        }
    }

    public function setImagesAttribute($value) {

        if (null != $value && is_array($value) ) {
            $images = [];
            foreach ($value as $image) {
                $images[] = uploadAllTyps($image, static::IMAGEPATH);
            }
            $this->attributes['images'] = $this->asJson($images);
        }
    }



    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }


    public static function boot() {
        parent::boot();
        /* creating, created, updating, updated, deleting, deleted, forceDeleted, restored */

        static::deleted(function($model) {
            if (isset($model->attributes['image'])) {
                deleteFile($model->attributes['image'] , static::IMAGEPATH);
            }
        });

    }
}
