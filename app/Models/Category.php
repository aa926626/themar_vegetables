<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Category extends BaseModel
{
    use HasTranslations;
    use HasFactory;

    const IMAGEPATH = 'categores';
    public $translatable = ['name'];
    protected $fillable = ['name', 'image', 'status'];
    public function status()
    {
        return $this->status ? 'Active' : 'Inactive';
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    public function scopeActive($query)
    {
        return $query->whereStatus(true);
    }

}

