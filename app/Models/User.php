<?php

namespace App\Models;


use App\Http\Resources\Api\Users\UserResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable , HasRoles;


    protected $guarded = ['id'];
    protected $hidden = ['password',];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_approved'       => 'boolean',
        'active'            => 'boolean',
        'is_blocked'        => 'boolean',
        'lat'               => 'decimal:8',
        'lng'               => 'decimal:8',

    ];
    public function setPhoneAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['phone'] = fixPhone($value);
        }
    }
    public function setCountryCodeAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['country_code'] = fixPhone($value);
        }
    }
    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $image = getImage($this->attributes['image'], 'users');
        } else {
            $image = defaultImage('users');
        }
        return $image;
    }
    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'], 'users') : '';
            $this->attributes['image'] = uploadAllTyps($value, 'users');
        }
    }
    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }
    public function sendVerificationCode()
    {

        $this->update([
            'code'        => mt_rand(1111, 9999),
            'code_expire' => Carbon::now()->addMinutes(10),
        ]);
        // send sms
    }
    public function markAsActive()
    {
        $this->update(['code' => null, 'code_expire' => null, 'active' => true]);
        return $this;
    }
    public function login()
    {

        $token = Auth::guard('api')->login($this);
        return UserResource::make($this)->setToken($token);
    }
    public function logout()
    {
        Auth::guard('api')->logout();
    }
    public function delegate()
    {
        return $this->hasOne(Delegate::class,'user_id');
    }
    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }
    public  function city(){
        return $this->belongsTo(City::class);
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public static function boot()
    {
        parent::boot();
        /* creating, created, updating, updated, deleting, deleted, forceDeleted, restored */

        static::deleted(function ($model) {
            deleteFile($model->attributes['image'], 'users');
        });
    }


}
