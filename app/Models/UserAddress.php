<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $casts = [
        'lat'               => 'decimal:8',
        'lng'               => 'decimal:8',
    ];
    public function setPhoneAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['phone'] = fixPhone($value);
        }
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function scopeUserId()
    {
        $user = auth()->user();
        $address = UserAddress::where('user_id', $user->id)->get();
    }
    public static function boot()
    {
        parent::boot();
        static::creating(function ($address) {
            $address->user_id = auth()->user()->id;
        });
    }

}
