<?php

namespace App\Http\Resources\Api\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersStatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'order_num' => $this->order_num,
            'total_products' => $this->total_products,
            'created_at' => $this->created_at->format('Y-m-d'),
            'status_text' => $this->status_text,
        ];
    }
}
