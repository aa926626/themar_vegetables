<?php

namespace App\Http\Resources\Api\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'UserName' => $this->user->name,
            'lat' => $this->userAddress->lat,
            'lng' => $this->userAddress->lng,
            'user_address_id' => $this->user_address_id,
            'total_products' => $this->total_products,
            'discount' => $this->discount,
            'total' => $this->total,
            'status_text' => $this->StatusText,
            'pay_type_text' => $this->pay_type_text,
            'pay_status_text' => $this->pay_status_text,
            'notes' => $this->notes,
        ];
    }
}
