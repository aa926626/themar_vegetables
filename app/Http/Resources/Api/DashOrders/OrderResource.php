<?php

namespace App\Http\Resources\Api\DashOrders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_number' => $this->order_num,
            'type' => $this->TypeText,
            'user_id' => $this->user_id,
            'UserName' => $this->user->name,
            'delegate_id' => $this->when($this->delegate_id, $this->delegate_id),
            'user_address_id' => $this->user_address_id,
            'User_city' => $this->userAddress->city,
            'user_lat' => $this->userAddress->lat,
            'user_lng' => $this->userAddress->lng,
            'user_map_desc' => $this->userAddress->map_desc,
            'user_address_type' => $this->userAddress->address_type,
            'total_products' => $this->total_products,
            'deliver_price' => $this->deliver_price,
            'discount_code' => $this->discount_code,
            'discount' => $this->discount,
            'vat_amount' => $this->vat_amount,
            'total' => $this->total,
            'status_text' => $this->StatusText,
            'preparation_status' => $this->preparation_status,
            'pay_type_text' => $this->pay_type_text,
            'pay_status_text' => $this->pay_status_text,
            'lat' => $this->when($this->lat, $this->lat),
            'lng' => $this->when($this->lng, $this->lng),
            'map_desc' => $this->when($this->map_desc, $this->map_desc),
            'date' => $this->date,
            'time' => $this->time,
            'notes' => $this->notes,
            'created_at' => $this->created_at,



        ];
    }
}
