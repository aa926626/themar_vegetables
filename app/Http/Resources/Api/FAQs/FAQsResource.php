<?php

namespace App\Http\Resources\Api\FAQs;

use Illuminate\Http\Resources\Json\JsonResource;

class FAQsResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'question' => $this->question,
            'answer' => $this->answer,
            'created_at' => $this->created_at->format('Y-m-d'),
            'updated_at' => $this->updated_at->format('Y-m-d'),
        ];
    }
}
