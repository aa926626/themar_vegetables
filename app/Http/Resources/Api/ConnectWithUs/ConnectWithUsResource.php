<?php

namespace App\Http\Resources\Api\ConnectWithUs;

use Illuminate\Http\Resources\Json\JsonResource;

class ConnectWithUsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'lat'        => $this->lat,
            'lng'        => $this->lng,
            'map_desc'   => $this->map_desc,
            'phone'      => $this->phone,
            'email'      => $this->email,
        ];
    }
}
