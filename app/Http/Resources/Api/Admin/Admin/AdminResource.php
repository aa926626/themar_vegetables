<?php

namespace App\Http\Resources\Api\Admin\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    private $token               = '';

    public function setToken($value) {
        $this->token = $value;
        return $this;
    }
    public function toArray($request)
    {
        return [
            'id'                              => $this->id,
            'name'                            => $this->name,
            'phone'                           => $this->phone,
            'email'                           => $this->email,
            'avatar'                          => $this->avatar,
            'is-blocked'                      => $this->is_blocked == 0 ? trans('auth.not_banned') : trans('auth.banned'),
            'token'                           => $this->when($this->token,$this->token),

        ];
    }
}
