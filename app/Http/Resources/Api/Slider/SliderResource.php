<?php

namespace App\Http\Resources\Api\Slider;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'image'                           => $this->image,
            'title'                           => $this->title,
            'description'                     => $this->description ?? '',
            'status'                          => $this->status == 1 ? trans('apis.Enabled') : trans('apis.Disabled'),
        ];
    }
}
