<?php

namespace App\Http\Resources\Api\Offers;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'type'                => $this->type,
            'image'               => $this->image,
            'max_use'             => $this->max_use,
            'use_times'           => $this->use_times,
            'user_times'          => $this->user_times,
            'amount'              => $this->amount,
            'max_discount'        => $this->max_discount,
            'amount_type'         => $this->amount_type,
            'can_use_with_coupon' => $this->can_use_with_coupon ? true : false,
            'product_use'         => $this->product_use,
            'start_date'          =>  $this->start_date,
            'expire_date'         => $this->expire_date,
            'status'              => $this->status ? true : false,
            'created_at'          => $this->created_at,
        ];
    }
}
