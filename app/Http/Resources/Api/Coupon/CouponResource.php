<?php

namespace App\Http\Resources\Api\Coupon;

use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'coupon_num' => $this->coupon_num,
            'type' => $this->type,
            'discount' => $this->discount ,
            'max_discount' => $this->max_discount,
            'expire_date' => $this->expire_date,
            'max_use' => $this->max_use,
            'use_times' => $this->use_times,
            'status' => $this->status,
            'created_at' => $this->created_at,

        ];
    }
}
