<?php

namespace App\Http\Resources\Api\LogActivity;

use App\Http\Resources\Api\Admin\Admin\AdminResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LogActivityResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'url' => $this->url,
            'method' => $this->method,
            'ip' => $this->ip,
            'agent' => $this->agent,
            'created_at' => $this->created_at,
            'Dashboard'    => new AdminResource($this->admin),

        ];
    }
}
