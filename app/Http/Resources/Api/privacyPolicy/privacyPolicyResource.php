<?php

namespace App\Http\Resources\Api\privacyPolicy;

use Illuminate\Http\Resources\Json\JsonResource;

class privacyPolicyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
          'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'created_at' => $this->created_at->format('Y-m-d'),
            'updated_at' => $this->updated_at->format('Y-m-d '),
        ];
    }
}
