<?php

namespace App\Http\Resources\Api\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    private $token               = '';

    public function setToken($value) {
        $this->token = $value;
        return $this;
    }

    public function toArray($request)
    {
        return [
            'id'                              => $this->id,
            'name'                            => $this->name,
            'phone'                           => $this->phone,
            'country_code'                    => $this->country_code,
            'user_type'                       => $this->user_type,
            'email'                           => $this->email,
            'lang'                            => $this->lang,
            'city_id'                         => $this->city?$this->city->id:null ,
            'city_name'                       => $this->city?$this->city->name:null,
            'active'                          => $this->active ? trans('apis.Enabled') : trans('apis.Disabled'),
            'is_blocked'                      => $this->is_blocked ? trans('apis.blocked') : trans('apis.unblocked'),
            'is_approved'                     => $this->is_approved ? trans('apis.approved') : trans('apis.not_approved'),
            'id_number'                       => $this->id_number,
            'image'                           => $this->image,
            'photo_driving_license'           => $this->when($this->delegate,$this->delegate->photo_driving_license ?? null),
            'photo_car_application'           => $this->when($this->delegate,$this->delegate->photo_car_application ?? null),
            'photo_car_insurance'             => $this->when($this->delegate,$this->delegate->photo_car_insurance   ?? null),
            'from_forward'                    => $this->when($this->delegate,$this->delegate->from_forward          ?? null),
            'from_behind'                     => $this->when($this->delegate,$this->delegate->from_behind           ?? null),
            'kind_of_car'                     => $this->when($this->delegate,$this->delegate->kind_of_car           ?? null),
            'car_model'                       => $this->when($this->delegate,$this->delegate->car_model             ?? null),
            'iban_number'                     => $this->when($this->delegate,$this->delegate->iban_number           ?? null),
            'bank_name'                       => $this->when($this->delegate,$this->delegate->bank_name             ?? null),
            'map_desc'                        => $this->map_desc ,
            'lat'                             => $this->lat,
            'lng'                             => $this->lng,
            'token'                           => $this->when($this->token,$this->token),
        ];
    }
}
