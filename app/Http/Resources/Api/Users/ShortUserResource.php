<?php

namespace App\Http\Resources\Api\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class ShortUserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                              => $this->id,
            'name'                            => $this->name,
            'phone'                           => $this->phone,
            'image'                           => $this->image,
        ];
    }
}
