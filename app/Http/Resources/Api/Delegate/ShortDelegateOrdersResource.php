<?php

namespace App\Http\Resources\Api\Delegate;

use App\Http\Resources\Api\Users\ShortUserResource;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ShortDelegateOrdersResource extends JsonResource
{
    public function toArray($request)
    {
        $order    = Order::where(['id'=>$this->id])->with(['user', 'userAddress'])->first();

        return [
            'id'              => $this->id,
           'address'         => $order->userAddress->map_desc,
            'status_text'     => $order->StatusText,
            'preparation_status' => trans('order.preparation_' . $order->preparation_status),
            'created'         => Carbon::parse($this->created_at)->format('Y/m/d'),
            'total'           => $order->total,
            'User'            => new ShortUserResource($order->user),

        ];

    }
}

