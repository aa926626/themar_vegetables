<?php

namespace App\Http\Resources\Api\Delegate;

use App\Http\Resources\Api\Users\ShortUserResource;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class DelegateOrdersResource extends JsonResource
{
    public function toArray($request)
    {
        $order    = Order::where(['id'=>$this->id])->with(['user', 'userAddress'])->first();

        $distance = getDistance ($order->userAddress,$request->lat,$request->lng);


        return [
            'id'              => $this->id,
            'address'         => $order->userAddress->map_desc,
            'status_text'     => $order->StatusText,
            'created'         => Carbon::parse($this->created_at)->format('Y/m/d'),
            'total'           => $order->total,
            'distance'        => $this->when($distance,numberFormat($distance->distance) ?? null) ,
            'User'            => new ShortUserResource($order->user),

        ];

    }
}

