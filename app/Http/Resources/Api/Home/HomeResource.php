<?php

namespace App\Http\Resources\Api\Home;

use App\Http\Resources\Api\Categories\CategoreResource;
use App\Http\Resources\Api\Products\ProductsResource;
use App\Http\Resources\Api\Slider\SliderResource;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'sliders' => SliderResource::collection($this->sliders),
            'categories' => CategoreResource::collection($this->categories),
            'products' => ProductsResource::collection($this->products),
        ];

    }
}
