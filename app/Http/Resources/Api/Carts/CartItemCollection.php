<?php

namespace App\Http\Resources\Api\Carts;

use App\Http\Resources\Api\Categories\CategoreResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CartItemCollection extends CartItemResource
{

    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
