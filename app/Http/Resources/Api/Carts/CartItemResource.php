<?php

namespace App\Http\Resources\Api\Carts;

use App\Http\Resources\Api\Media\MediCollection;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $product = Product::find($this->product_id);

        return [
            'price'     => $product->price,
            'name'      => $product->name,
            'Quantity'  => $this->quantity,
            'image' => asset('storage/images/' . 'products' . '/' . $product->firstMedia->file_name),

        ];

    }
}
