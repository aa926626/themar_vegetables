<?php

namespace App\Http\Resources\Api\Address;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address_type' => $this->address_type,
            'city' => $this->city,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'map_desc' => $this->map_desc,
            'phone' => $this->phone,
            'description' => $this->description,

        ];
    }
}
