<?php

namespace App\Http\Resources\Api\Products;

use App\Http\Resources\Api\Categories\CategoreResource;
use App\Http\Resources\Api\Media\MediaResource;
use App\Http\Resources\Api\Media\MediCollection;
use App\Http\Resources\Api\Offer\OfferResource;
use App\Http\Resources\Api\Review\ReviewResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ImagesProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'category' => $this->category->name,
            'image' => MediCollection::collection($this->media),
            'Reviews' => $this->when($this->reviews->count() > 0, ReviewResource::collection($this->reviews)),
        ];
    }
}

