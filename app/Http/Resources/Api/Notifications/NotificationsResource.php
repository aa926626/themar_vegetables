<?php

namespace App\Http\Resources\Api\Notifications;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationsResource extends JsonResource {

    public function toArray($request) {
        return [
            'id'    => $this->id,
            'type'  => $this->data['type'],
            'order_id'    => $this->data['order_id'],
            'order_status'=> $this->data['order_status'],
            'time'  => $this->created_at->format('Y-m-d'),
        ];
    }
}
