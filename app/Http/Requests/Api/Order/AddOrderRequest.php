<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\BaseApiRequest;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'user_address_id' => 'required|exists:user_addresses,id',
            'pay_type' => 'required|in:0,1,2',
            'notes' => 'nullable',
            'date' => 'required|after_or_equal:today',
            'time' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'date' => Carbon::parse(convert2english($this->date))->format('Y-m-d'),
        ]);
    }
}
