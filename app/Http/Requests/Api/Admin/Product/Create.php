<?php

namespace App\Http\Requests\Api\Admin\Product;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends BaseApiRequest
{
    public function rules() {
        return [

                'name.ar'                  => 'required|max:191',
                'name.en'                  => 'required|max:191',
                'description.ar'           =>  'nullable|max:300',
                'description.en'           =>  'nullable|max:300',
                'price'                    => 'required|numeric',
                'quantity'                 => 'required|numeric',
                'category_id'              => 'required|exists:categories,id',
                'featured'                 => 'nullable|in:0,1',
                'status'                   => 'required|in:0,1',
                'images'                   => 'required|array',
        ];
    }
}

