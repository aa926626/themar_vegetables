<?php

namespace App\Http\Requests\Api\Admin\Category;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends BaseApiRequest
{
    public function rules() {
        return [

            'name.ar'                  => 'required|max:191',
            'name.en'                  => 'required|max:191',
            'status'                   => 'nullable|in:0,1',
            'image'                    => ['nullable','image'],
        ];
    }
}

