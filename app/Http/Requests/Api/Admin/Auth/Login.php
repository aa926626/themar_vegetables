<?php

namespace App\Http\Requests\Api\Admin\Auth;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class Login extends BaseApiRequest {

    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
    }

    public function rules() {
        return [
        'email' => 'required|email|exists:admins,email|max:50',
        'password' => 'required|min:6|max:100',
        ];
    }
}
