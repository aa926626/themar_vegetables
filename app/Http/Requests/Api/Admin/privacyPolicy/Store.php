<?php

namespace App\Http\Requests\Api\Admin\privacyPolicy;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{

    public function rules()
    {
        return [
            'title.ar' => 'required|string',
            'title.en' => 'required|string',
            'description.ar' => 'required|string',
            'description.en' => 'required|string',
        ];
    }
}
