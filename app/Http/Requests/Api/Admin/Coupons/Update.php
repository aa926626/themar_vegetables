<?php

namespace App\Http\Requests\Api\Admin\Coupons;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{

    public function rules()
    {
        return [
            'coupon_num'   => 'required|string',
            'type'         => 'nullable|in:ratio,number',
            'discount'     => 'required|numeric',
            'max_discount' => 'required|numeric',
            'expire_date'  => 'required|date',
            'max_use'      => 'required|numeric',
            'status'       => 'nullable|in:available,expire,usage_end,closed',
        ];
    }
}
