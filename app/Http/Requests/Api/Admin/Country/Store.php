<?php

namespace App\Http\Requests\Api\Admin\Country;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{
    public function rules()
    {
        return [
            'name.ar' => 'required',
            'name.en' => 'required',
            'key'     => 'required',
        ];
    }
}
