<?php

namespace App\Http\Requests\Api\Admin\FAQS;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{

    public function rules()
    {
        return [
            'question.ar' => 'required|max:255',
            'question.en' => 'required|max:255',
            'answer.ar' => 'required|max:255',
            'answer.en' => 'required|max:255',
        ];
    }
}
