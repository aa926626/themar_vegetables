<?php

namespace App\Http\Requests\Api\Admin\ConnectMessage;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{

    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'message' => 'required|string',
        ];
    }
}
