<?php

namespace App\Http\Requests\Api\Admin\City;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{
    public function rules()
    {
        return [
            'name.ar' => 'required',
            'name.en' => 'required',
            'country_id' => 'required|exists:countries,id',

        ];
    }
}
