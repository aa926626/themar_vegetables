<?php

namespace App\Http\Requests\Api\Admin\Orders;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{

    public function rules()
    {
        return [
            'status' => 'required|in:0,1,2,3,4,5,6,7,8,9,10,11,12,13',
            'delegate_id' => 'required|exists:users,id',
        ];
    }
}
