<?php

namespace App\Http\Requests\Api\Admin\Offers;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{

    public function rules()
    {
        return [
            'type'                => 'required|in:Products,Categories',
            'max_use'             => 'required|integer',
            'image'               => 'nullable|image'   ,
            'use_times'           => 'required|integer',
            'amount'              => 'required|numeric',
            'max_discount'        => 'required|numeric',
            'amount_type'         => 'required|in:ratio,number',
            'can_use_with_coupon' => 'required|in:1,0',
            'start_date'          => 'required|date',
            'expire_date'         => 'required|date',
            'status'              => 'required|in:1,0',
        ];
    }
}
