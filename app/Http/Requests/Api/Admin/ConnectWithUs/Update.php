<?php

namespace App\Http\Requests\Api\Admin\ConnectWithUs;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{
    public function rules()
    {
        return [
            'lat'       => 'nullable',
            'lng'       => 'nullable',
            'map_desc'  => 'nullable',
            'phone'     => 'nullable|numeric,' . $this->id,
            'email'     => 'nullable|email,' . $this->id,
            'active'    => 'nullable|in:0,1'
        ];
    }
}
