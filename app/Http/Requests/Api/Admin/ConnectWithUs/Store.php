<?php

namespace App\Http\Requests\Api\Admin\ConnectWithUs;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{
    public function rules()
    {
        return [
            'lat'       => 'required',
            'lng'       => 'required',
            'map_desc'  => 'required',
            'phone'     => 'required|numeric',
            'email'     => 'required|email',
            'active'    => 'nullable|in:0,1'
        ];
    }
}
