<?php

namespace App\Http\Requests\Api\Admin\Admin;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends BaseApiRequest
{
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
    }


    public function rules(): array
    {
        return [
            'name'     => 'required|max:191',
            'phone'    => "required|min:10|unique:admins,phone," . $this->id,
            'email'    => "required|email|max:191|unique:admins,email," . $this->id,
            'password' => 'nullable|min:6|max:255',
            'avatar'   => 'nullable|image',
            'role_id'  => 'required|exists:roles,id',
            'active'   => 'nullable|in:1,0',
            'is_blocked'  => 'required|in:1,0',
        ];
    }
}

