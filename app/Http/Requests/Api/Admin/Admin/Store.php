<?php

namespace App\Http\Requests\Api\Admin\Admin;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Store extends BaseApiRequest
{
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
    }


    public function rules() {
        return [
            'name'     => 'required|max:191',
            'phone'    => 'required|min:10|unique:users,phone',
            'email'    => 'required|email|max:191|unique:admins,email',
            'password' => 'required|min:6|max:255',
            'avatar'   => 'nullable|image',
            'role_id'  => 'required|exists:roles,id',
            // 'is_blocked'  => 'required|in:0,1',
        ];
    }
}

