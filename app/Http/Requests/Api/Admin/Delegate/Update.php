<?php

namespace App\Http\Requests\Api\Admin\Delegate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
        $request['country_code'] = fixPhone($request['country_code']);
    }
    public function rules()
    {
        return [
            'name'                        => 'required|max:50',
            'country_code'                => 'nullable|numeric|digits_between:2,5',
            'phone'                       => 'nullable|numeric|digits_between:9,10|unique:users,phone,'.$this->id,
            'id_number'                   => 'nullable|numeric|digits:14|unique:users,id_number,'.$this->id,
            'city'                        => 'required|max:50',
            'city_id'                     => 'required|exists:cities,id',
            'lat'                         => 'required',
            'lng'                         => 'required',
            'map_desc'                    => 'required',
            'email'                       => 'nullable|email|unique:users,email|max:50,' . $this->id,
            'password'                    => 'nullable|min:6|max:100',
            'image'                       => 'nullable',
            'photo_driving_license'       => 'nullable|image',
            'photo_car_application'       => 'nullable|image',
            'photo_car_insurance'         => 'nullable|image',
            'from_forward'                => 'nullable|image',
            'from_behind'                 => 'nullable|image',
            'kind_of_car'                 => 'required',
            'car_model'                   => 'required',
            'iban_number'                 => 'nullable|numeric',
            'bank_name'                   => 'required',
            'active'                      => 'nullable|in:0,1',
            'is_blocked'                  => 'nullable|in:0,1',
            'is_verified'                 => 'nullable|in:0,1',
        ];

    }
}
