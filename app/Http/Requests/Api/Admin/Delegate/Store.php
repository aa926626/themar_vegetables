<?php

namespace App\Http\Requests\Api\Admin\Delegate;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;



class Store extends BaseApiRequest
{
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
        $request['country_code'] = fixPhone($request['country_code']);
    }

    public function rules()
    {
        return [
            'name'                        => 'required|max:50',
            'country_code'                => 'nullable|numeric|digits_between:2,5',
            'phone'                       => 'required|numeric|digits_between:9,10|unique:users,phone',
            'id_number'                   => 'required|numeric|digits:14|unique:users,id_number',
            'city'                        => 'required|max:50',
            'city_id'                     => 'required|exists:cities,id',
            'lat'                         => 'required',
            'lng'                         => 'required',
            'map_desc'                    => 'required',
            'email'                       => 'required|email|unique:users,email|max:50',
            'password'                    => 'required|min:6|max:100',
            'image'                       => 'nullable',
            'photo_driving_license'       => 'required|image',
            'photo_car_application'       => 'required|image',
            'photo_car_insurance'         => 'required|image',
            'from_forward'                => 'required|image',
            'from_behind'                 => 'required|image',
            'kind_of_car'                 => 'required',
            'car_model'                   => 'required',
            'iban_number'                 => 'required|numeric',
            'bank_name'                   => 'required',
            'active'                      => 'nullable|in:0,1',
            'is_blocked'                  => 'nullable|in:0,1',
            'is_verified'                 => 'nullable|in:0,1',
        ];
    }
}
