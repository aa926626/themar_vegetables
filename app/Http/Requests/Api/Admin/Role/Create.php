<?php

namespace App\Http\Requests\Api\Admin\Role;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Create extends BaseApiRequest
{

    public function rules()
    {
        return [
            'name'       => 'required|max:191',
//            'name.en'       => 'required|max:191',
            'permissions'   => 'nullable'
        ];
    }
}
