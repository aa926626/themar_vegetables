<?php

namespace App\Http\Requests\Api\Admin\Sliders;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{
    public function rules()
    {
        return [
            'title.ar' => 'required|string',
            'title.en' => 'required|string',
            'description.ar' => 'nullable|string',
            'description.en' => 'nullable|string',
            'status' => 'nullable|boolean',
            'image' => 'nullable|image',
        ];
    }
}
