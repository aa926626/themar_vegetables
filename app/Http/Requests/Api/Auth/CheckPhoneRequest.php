<?php

namespace App\Http\Requests\Api\Auth;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class CheckPhoneRequest extends BaseApiRequest {
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
    }

    public function rules() {
        return [
            'phone'        => 'required|numeric|digits_between:9,10',
        ];
    }
}
