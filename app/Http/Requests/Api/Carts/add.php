<?php

namespace App\Http\Requests\Api\Carts;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class add extends BaseApiRequest {

    public function rules() {
        return [
//            'cartKey' => 'required',
            'product_id' => 'required',
            'quantity' => 'required|numeric|min:1',
        ];
    }
}

