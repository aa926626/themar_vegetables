<?php

namespace App\Http\Requests\Api\Carts;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class Update extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
//            'cartKey' => 'required',
            'quantity' => 'required|numeric|min:1|max:10',
            'product_id' => 'required',
        ];

    }
}
