<?php

namespace App\Http\Requests\Api\Review;

use App\Http\Requests\Api\BaseApiRequest;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class Store extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'required|numeric|min:1|max:5',
            'review' => 'required|string',
        ];
    }


}
