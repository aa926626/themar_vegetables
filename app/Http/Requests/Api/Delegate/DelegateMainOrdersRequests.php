<?php

namespace App\Http\Requests\Api\Delegate;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class DelegateMainOrdersRequests extends BaseApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'lat'      => 'required',
            'lng'      => 'required',
        ];
    }
}
