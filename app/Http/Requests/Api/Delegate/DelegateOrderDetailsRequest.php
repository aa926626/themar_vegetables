<?php

namespace App\Http\Requests\Api\Delegate;
use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class DelegateOrderDetailsRequest extends BaseApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'status'   => 'nullable|in:6,7,8,9,10,11,12',
            'lat'      => 'required',
            'lng'      => 'required',
            'reason'   => 'required_if:status,==,7',
        ];
    }
}
