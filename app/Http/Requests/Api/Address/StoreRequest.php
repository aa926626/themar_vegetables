<?php

namespace App\Http\Requests\Api\Address;

use App\Http\Requests\Api\BaseApiRequest;
use Illuminate\Http\Request;

class StoreRequest extends BaseApiRequest {
    public function __construct(Request $request) {
        $request['phone']        = fixPhone($request['phone']);
    }

    public function rules() {
        return [
            'address_type' => 'required|in:home,work',
            'city'         => 'required|max:50',
            'lat'          => 'required',
            'lng'          => 'required',
            'map_desc'     => 'required',
            'phone'        => 'required|numeric|digits:10',
        ];
    }
}
