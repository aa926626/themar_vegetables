<?php

namespace App\Http\Controllers\Api\Logic\Delegate;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Delegate\DelegateMainOrdersRequests;
use App\Http\Requests\Api\Delegate\DelegateOrderDetailsRequest;
use App\Http\Resources\Api\Delegate\DelegateOrdersResource;
use App\Http\Resources\Api\Delegate\ShortDelegateOrdersResource;
use App\Models\Order;
use App\Models\OrderDelegate;
use App\Notifications\OrderNotification;
use App\Services\OrderService;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Validator;


class OrderDelegateController extends Controller
{
    use ResponseTrait, PaginationTrait;

    public $orderData;

    public function __construct( OrderService $orderData){
        $this->OrderService    = $orderData;
    }
    public function DelegateMainOrders(DelegateMainOrdersRequests $request)
    {
            $ordersIds  = OrderDelegate::where(['delegate_id'=>Auth::id()])->whereNull('status')->pluck('order_id')->toArray();
            $orders     = Order::whereIn      ('id',$ordersIds)->with(['user', 'userAddress'])->first();
            if ($orders == null){return $this->successMsg('لا يوجد طلبات جديدة');}
            $distances  = getDistanceHaving   ($orders->userAddress,$request->lat,$request->lng);
            $data       = DelegateOrdersResource::collection($distances);
            return     $this->successData    (['orders' => $data]);
    }
    public function DelegateOrderDetails(DelegateOrderDetailsRequest $request)
    {
        $order = Order::where(['id' => $request->order_id])->with(['user', 'userAddress'])->first();
        if (isset($request->status)) {
            if ($request['status'] < $order->status) {
                return $this->response('fail', __('apis.invalid_order_status'), []);
            }
        }
        if (!isset($order)) {
            return $this->failMsg(trans('apis.data_incorrect'));
        }
        $orderDelegate = OrderDelegate::where(['delegate_id' => Auth::id(), 'order_id' => $order->id])->first();
        // Accepted Order
        if (isset($request->status) && $request->status == 6){
            if ($orderDelegate->status == 'accepted')
            {
                return $this->successMsg(trans('apis.order_already_accepted') ." " .  $order->delegate->name );
            }
            $orderDelegate->update(['status'=>'accepted']);
            $order->update(['delegate_id'=> Auth::id()]);
            OrderDelegate::where('order_id',$order->id)->whereNull('status')->delete();
            Notification::send($order->user, new OrderNotification($order->refresh(),$order->user,'DELEGATE_ACCEPTED'));
        }

        //Delegate refused Order
        if (isset($request->status) && $request->status == 7) {
            if ($orderDelegate->status == 'accepted') {
                return $this->failMsg(trans('apis.order_already_accepted'));
            }
            $orderDelegate->update(['status' => 'refused', 'reason' => $request->reason]);
            Notification::send($order->user, new OrderNotification($order->refresh(), $order->user, 'DELEGATE_CANCELLED'));
        }

        #wallet_balance
        if (!in_array($order->pay_type,[1]) && $request->status == 12){
            Auth::user()->increment('wallet_balance',$order->deliver_price);
        }

        if (isset($request->status)) {
            $order->update(['status' => $request->status]);
        }

        $distance = getDistance($order->userAddress, $request->lat, $request->lng);
        $data = $this->OrderService->delegateOrderDetails($order);
        $data['distance'] = numberFormat($distance->distance);
        return $this->successData($data);
    }
    public function DelegatePendingOrders(Request $request)
    {
        $orders      = Order::where('delegate_id',Auth::id())->whereIn('status',[6,8,9,10,11,12])->paginate(10);
        $data        = ShortDelegateOrdersResource::collection($orders);
        $pagination  = $this->paginationModel($orders);
        return       $this->successData(['orders' => $data]);
    }
    public function DelegateFinishedOrders()
    {
        $orders = Order::where('delegate_id',Auth::id())->whereIn('status',[5,13])->paginate(10);
        $data   = ShortDelegateOrdersResource::collection($orders);
        $pagination = $this->paginationModel($orders);
        return $this->successData(['pagination' => $pagination,'orders' => $data]);
    }
    public function DelegateDeliveryStart(Request $request)
    {

        $order = Order::where(['id' => $request->order_id])->with(['user', 'userAddress'])->first();
        if ($order->status >= 6 && $order->preparation_status == "done") {
            $order->update(['status' => 12]);
            return $this->successMsg('تم بدء التوصيل');
            Notification::send($order->user, new OrderNotification($order->refresh(), $order->user, 'DELEGATE_DELIVERY_START'));
        }
        return $this->failMsg('الاوردر في  حاله  التحضير ');
    }
    public function DelegateDeliveryEnd(Request $request)
    {
        $order = Order::where(['id' => $request->order_id])->with(['user', 'userAddress'])->first();
        if ($order == null) {
            return $this->failMsg('لا يوجد اوردر');
        }
        if ($order->status == 12) {
            $order->update(['status' => 13]);
            return $this->successMsg('تم التوصيل');
            Notification::send($order->user, new OrderNotification($order->refresh(), $order->user, 'DELEGATE_DELIVERY_END'));
        }
        return $this->failMsg('هناك خطأ في الاوردر');
    }
    public function DelegateSearchOrders(Request $request)
    {
        $orders = Order::where('delegate_id',Auth::id())->where(function ($q) use ($request) {
            $q->where('id', $request->search)->orWhereHas('user', function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%');
            });
        })->paginate(10);
        $data = ShortDelegateOrdersResource::collection($orders);
        $pagination = $this->paginationModel($orders);
        return $this->successData(['pagination' => $pagination,'orders' => $data]);
    }

    public function ShowOrderDelegate(Request $request)
    {
        $order = Order::where(['delegate_id' => Auth::id(), 'id' => $request->order_id])->with(['user', 'userAddress'])->first();
        if ($order == null) {
            return $this->failMsg('لا يوجد اوردر');
        }
        $data = $this->OrderService->delegateOrderDetails($order);
        return $this->successData($data);
    }


}



