<?php

namespace App\Http\Controllers\Api\Logic\Users;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Order\AddOrderRequest;
use App\Http\Resources\Api\Notifications\NotificationsResource;
use App\Http\Resources\Api\Orders\OrderResource;
use App\Http\Resources\Api\Orders\OrdersStatusResource;
use App\Models\Order;
use App\Models\OrderDelegate;
use App\Models\User;
use App\Models\UserAddress;
use App\Services\OrderService;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;
use Auth;
use Illuminate\Http\Request;


class UserController extends Controller
{
use ResponseTrait , PaginationTrait;
    //api checkout

    public  $transaction;
    public function __construct(OrderService $orderData){

        $this->OrderService       = $orderData;
    }
    public function storeOrder(AddOrderRequest $request)
    {
        $user        = auth()->guard('api')->user();
        $order       = $this->OrderService->createOrder($request->validated() + ['coupon_num' => $request->coupon_num ?? '1Null1'] ,$user);
        $userAddress = UserAddress::where('user_id', auth()->guard('api')->user()->id)->first();
        $distances   = User::where(['user_type'=>'delegate','is_approved'=>1])->get();
        foreach      ($distances as $delegate){
            OrderDelegate::updateOrCreate([
                'delegate_id' => $delegate->id,
                'order_id'    => $order->id,
            ],[
                'delegate_id' => $delegate->id,
                'order_id'    => $order->id,
            ]);
        }
        $orders      = new OrderResource($order);
        return $this->successData($orders);
    }
    public function showOrder(Request $request)
    {
        $order = Order::where('user_id', auth()->guard('api')->user()->id)->get();
        return $this->successData(OrderResource::collection($order));
    }
    public function deleteOrder(Request $request)
    {
        $order = Order::where('user_id', auth()->guard('api')->user()->id)->where('id', $request->order_id)->first();
        if (!isset($order)){
            return $this->failMsg(trans('apis.data_incorrect'));
        }
        $order->delete();
        return $this->response('success',trans('apis.deleted'));

    }
    public function userPendingOrders()
    {
        $orders = Order::where('user_id',Auth::id())->where('pay_type','<>',0)
            ->whereNotIn('status',[13,1,3])->paginate(10);
        $data   = OrdersStatusResource::collection($orders);

        $pagination = $this->paginationModel($orders);

        return $this->successData(['pagination'=>$pagination,'orders' => $data]);
    }
    public function userFinishedOrders()
    {
        $orders = Order::where('user_id',Auth::id())->whereIn('status',[13,1,3])->paginate(10);

        $data  = OrdersStatusResource::collection($orders);

        $pagination = $this->paginationModel($orders);

        return $this->successData(['pagination'=>$pagination,'orders' => $data]);
    }
    public function userOrderDetails(Request $request , $id){

        $order = Order::where('user_id', auth()->guard('api')->user()->id)->where('id', $id)->first();
        if (!isset($order)){
            return $this->failMsg(trans('apis.data_incorrect'));
        }
        return $this->successData(new OrderResource($order));
    }
    public  function Notifications(Request $request){
        auth()->user()->unreadNotifications->markAsRead();
        $notifications = NotificationsResource::collection(auth()->user()->notifications()->paginate(20));
        $pagination = $this->paginationModel($notifications);
        return $this->successData(['pagination' => $pagination, 'notifications' => $notifications]);
    }
    public function countUnreadNotifications()
    {
        return $this->successData(['count' => auth()->user()->unreadNotifications->count()]);
    }

    public function deleteNotification($notification_id)
    {
        auth()->user()->notifications()->where('id', $notification_id)->delete();
        return $this->successMsg(__('site.notify_deleted'));
    }

    public function deleteNotifications()
    {
        auth()->user()->notifications()->delete();
        return $this->successMsg(__('apis.deleted'));
    }
}

