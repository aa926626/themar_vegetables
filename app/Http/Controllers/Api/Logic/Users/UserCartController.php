<?php

namespace App\Http\Controllers\Api\Logic\Users;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Carts\add;
use App\Http\Requests\Api\Carts\Update;
use App\Http\Resources\Api\Carts\CartItemResource;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\Product;
use App\Traits\ResponseTrait;
use Auth;
use Illuminate\Http\Request;


class UserCartController extends Controller
{
    use ResponseTrait;

    public function store(Request $request)
    {
        if (Auth::guard('api')->check()) {$user_id = Auth::guard('api')->user()->id;}
        if (Cart::where('user_id', $user_id)->exists()) {return $this->successMsg('لديك سلة مفعلة بالفعل');}
        $cart = Cart::create(['user_id' => $user_id,]);
        return $this->response('success', 'تم إنشاء عربة جديدة من أجلك!', ['cartToken' => $cart->id, 'cartKey' => $cart->key]);
    }

    public function addProducts(add $request)
    {
        $cart = Cart::where('user_id',Auth::guard('api')->user()->id )->first();
        $product = Product::where('id', $request->product_id)->first();
        if ($product->quantity < $request->quantity) {return $this->failMsg('الكمية غير متوفرة');}
        if (CartItem::where('product_id' , $request->product_id)->exists()) {return $this->successMsg('هذا المنتج مضاف مسبقا للسله'); }
         CartItem::create($request->validated() + ['product_id' => $product->id, 'cart_id' => $cart->id,]);
        return $this->response('success', 'تم إضافة المنتج إلى العربة!', ['cartToken' => $cart->id, 'cartKey' => $cart->key]);
    }

    public function CartItemShow(Request $request)
    {
        $user_id = Auth::guard('api')->user()->id;
        $carts = Cart::where('user_id', $user_id)->first();
        if ($carts == null) {return $this->failMsg('لا يوجد عربة مفعلة');}
        return $this->successData(CartItemResource::collection($carts->CartItem()->get()));
    }

    public function update(Update $request)
    {
        $cart = Cart::where('user_id',Auth::guard('api')->user()->id )->first();
        $product = Product::where('id', $request->product_id)->first();
        if ($product->quantity < $request->quantity) {return $this->failMsg('الكمية غير متوفرة');}
        $cartItem = CartItem::where('product_id', $request->product_id)->first();
        $cartItem->update($request->validated());
        return $this->response('success', 'تم تحديث العنصر بنجاح!', ['cartToken' => $cart->id]);
    }

    public function destroy(Request $request)
    {
        $cart = Cart::where('user_id',Auth::guard('api')->user()->id )->first();
        if (CartItem::where('product_id', $request->product_id)->exists()) {
            $cartItem = CartItem::where('product_id', $request->product_id)->first();
            $cartItem->delete();
            return $this->successMsg('تم حذف  المنتج من السلة');
        } else {
            return $this->failMsg('هذا المنتج غير موجود في السلة');
        }
    }

    public function destroyAll(Request $request)
    {
//      dd(substr(exec('getmac'), 0, 17));
        $cart = Cart::where('user_id',Auth::guard('api')->user()->id )->first();
        if (CartItem::where('cart_id', $cart->id)->exists()) {
            $cartItem = CartItem::where('cart_id', $cart->id)->get();
            $cartItem->each->delete();
            return $this->successMsg('تم حذف  جميع المنتجات من السلة');
        } else {
            return $this->failMsg('لا يوجد منتجات في السلة');
        }
    }

    public function count(Request $request)
    {
        $cart = Cart::where('user_id', Auth::guard('api')->user()->id)->first();
        $cartItem = CartItem::where('cart_id', $cart->id)->get();
        $total = 0;
        foreach ($cartItem as $item) {
            $total += $item->product->price * $item->quantity;
        }
        return $this->successData(['total' => $total]);
       }

    public function discountCoupon(Request $request){
        $coupon = Coupon::where('coupon_num', $request->coupon_num)->first();
        if ($coupon == null) {return $this->failMsg('الكوبون غير صالح');}
        if ($coupon->expire_date < now()) {return $this->failMsg('الكوبون منتهي الصلاحية');}
        if ($coupon->max_use == 0) {return $this->failMsg('الكوبون غير متوفر');}
        if ($coupon->status != 'available') {return $this->failMsg('الكوبون غير مفعل');}
        if ($coupon->use_times == $coupon->max_use) {return $this->failMsg('الكوبون غير متوفر');}
        $cart = Cart::where('user_id', Auth::guard('api')->user()->id)->first();
        $cartItem = CartItem::where('cart_id', $cart->id)->get();
        $total = 0;
        foreach ($cartItem as $item) {
            $total += $item->product->price * $item->quantity;
        }
        if ($coupon->type == 'ratio') {
            $total = $total - ($total * $coupon->discount / 100);
        } else {
            $total = $total - $coupon->discount;
        }
        return $this->successData(['discount' => $coupon->type .  $coupon->discount  , 'total' => $total]);
    }





}
