<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Sliders\Store;
use App\Http\Requests\Api\Admin\Sliders\Update;
use App\Http\Resources\Api\Slider\SliderResource;
use App\Models\Slider;
use App\Traits\Report;
use App\Traits\ResponseTrait;

class SliderController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $slider = Slider::paginate(10);
        $sliders = SliderResource::collection($slider);
        return $this->successData(['Sliders' => $sliders]);
    }

    public function show($id)
    {
        $slider = Slider::find($id);
        if (!$slider)
            return $this->failMsg('هذا السلايدر غير موجود');
        $resource = new SliderResource($slider);
        return $this->successData(['Slider' => $resource]);
    }

    public function store(Store $request)
    {
        $slider = Slider::create($request->validated());
        $resource = new SliderResource($slider);
        Report::addToLog(' اضافة سلايدر جديد');
        return $this->successData(['Slider' => $resource]);
    }

    public function update(Update $request , $id){
        $slider = Slider::find($id);
        if (!$slider)
            return $this->failMsg('هذا السلايدر غير موجود');
        $slider->update($request->validated());
        $resource = new SliderResource($slider);
        Report::addToLog(' تعديل سلايدر');
        return $this->successData(['Slider' => $resource]);
    }

    public function destroy($id){
        $slider = Slider::find($id);
        if (!$slider)
            return $this->failMsg('هذا السلايدر غير موجود');
        $slider->delete();
        Report::addToLog(' حذف سلايدر');
        return $this->successMsg('تم حذف السلايدر بنجاح');
    }

}
