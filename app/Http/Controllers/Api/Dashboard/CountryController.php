<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Country\Store;
use App\Http\Resources\Api\Country\CountryResource;
use App\Models\Country;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    use ResponseTrait , PaginationTrait;

    public function index(Request $request)
    {
        $countries = Country::paginate($request->limit ?? 10);
        $pagination = $this->paginationModel($countries);
        $response = CountryResource::collection($countries);
        return $this->successData(['countries' => $response , 'pagination' => $pagination]);
    }

    public function show(Country $country)
    {
        $response = new CountryResource($country);
        return $this->successData(['country' => $response]);
    }

    public function store(Store $request)
    {
        $country = Country::create($request->validated());
        $response = new CountryResource($country);
        Report::addToLog('باضافة دولة جديدة');
        return $this->successData(['country' => $response]);
    }

    public function update(Store $request , Country $country)
    {
        $country->update($request->validated());
        $response = new CountryResource($country);
        Report::addToLog('بتعديل بيانات دولة');
        return $this->successData(['country' => $response]);
    }

    public function destroy(Country $country)
    {
        $country->delete();
        Report::addToLog('بحذف دولة');
        return $this->successMsg('تم حذف الدولة بنجاح');
    }


}
