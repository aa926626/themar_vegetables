<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\City\Store;
use App\Http\Resources\Api\City\CityResource;
use App\Models\City;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CityController extends Controller
{
    use Report , ResponseTrait , PaginationTrait;


    public function index(Request $request)
    {
        $cities = City::paginate($request->limit ?? 10);
        $pagination = $this->paginationModel($cities);
        $response = CityResource::collection($cities);
        return $this->successData(['cities' => $response , 'pagination' => $pagination]);
    }

    public function show(City $city)
    {
        $response = new CityResource($city);
        return $this->successData(['city' => $response]);
    }

    public function store(Store $request)
    {
        $city = City::create($request->validated());
        Report::addToLog('بإضافة المدينة') ;
        $response = new CityResource($city);
        return $this->successData(['city' => $response]);
    }

    public function update(Store $request , City $city)
    {
        $city->update($request->validated());
        Report::addToLog('بتعديل المدينة') ;
        $response = new CityResource($city);
        return $this->successData(['city' => $response]);
    }

    public function destroy(City $city)
    {
        $city->delete();
        Report::addToLog('بحذف المدينة') ;
        return $this->successMsg('تم حذف المدينة بنجاح');
    }

}
