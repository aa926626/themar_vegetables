<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Api\LogActivity\LogActivityResource;
use App\Models\LogActivity;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class LogActivityController extends Controller
{
    use ResponseTrait , PaginationTrait;
    public function index(){
        $logActivities = LogActivity::with('admin')->latest()->paginate(10);
        $pagination = $this->paginationModel($logActivities);
        $response =  LogActivityResource::collection($logActivities);
        return $this->successData(['logActivities' => $response , 'pagination' => $pagination]);
    }

    public function show(LogActivity $logActivity){
        $response = new LogActivityResource($logActivity);
        return $this->successData(['logActivity' => $response]);
    }

    public function destroy(LogActivity $logActivity){
        $logActivity->delete();
        return $this->successMsg('تم حذف السجل بنجاح');
    }

    public function destroyAll(Request $request){
        LogActivity::whereIn('id' , $request->ids)->delete();
        return $this->successMsg('تم حذف السجلات بنجاح');
    }
}
