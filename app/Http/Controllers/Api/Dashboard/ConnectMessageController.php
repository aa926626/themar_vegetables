<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Api\ConnectMessage\ConnectMessageResource;
use App\Models\ConnectMessage;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ConnectMessageController extends Controller
{
    use ResponseTrait , PaginationTrait , Report;

    public function index()
    {
        $data = ConnectMessage::paginate(10);
        $pag = $this->paginationModel($data);
        $res =  ConnectMessageResource::collection($data);
        return $this->successData(['data' => $res, 'pag' => $pag]);
    }

    public function show(ConnectMessage $connectMessage)
    {
        $res = new ConnectMessageResource($connectMessage);
        return $this->successData($res);
    }

    public function destroy(ConnectMessage $connectMessage)
    {
        $connectMessage->delete();
        return $this->successMsg(__('apis.deleted'));
    }
    public function destroyAll(Request $request)
    {
        $ids = $request->ids;
        ConnectMessage::whereIn('id', $ids)->delete();
        return $this->successMsg(__('apis.deleted'));
    }

}
