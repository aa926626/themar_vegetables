<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\FAQS\Store;
use App\Http\Resources\Api\FAQs\FAQsResource;
use App\Models\FAQs;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class FAQSController extends Controller
{
    use ResponseTrait , PaginationTrait , Report;

    public function index(Request $request)
    {
        $data = FAQs::orderBy('id', 'desc')->paginate(10);
        $response = FAQsResource::collection($data);
        return $this->successData($response);
    }

    public function show(FAQs $faq)
    {
        $response = new FAQsResource($faq);
        return $this->successData($response);
    }

    public function store(Store $request)
    {
        $city = FAQs::create($request->validated());
        Report::addToLog('بإضافة سؤال') ;
        $response = new FAQsResource($city);
        return $this->successData($response);
    }

    public function update(Store $store , FAQs $faq)
    {
        $faq->update($store->validated());
        Report::addToLog('بتعديل سؤال') ;
        $response = new FAQsResource($faq);
        return $this->successData($response);
    }

    public function destroy(FAQs $faq)
    {
        $faq->delete();
        Report::addToLog('بحذف سؤال') ;
        return $this->successMsg('تم حذف السؤال بنجاح');
    }


}
//privacyPolicy
