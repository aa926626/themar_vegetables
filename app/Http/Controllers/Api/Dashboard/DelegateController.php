<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Delegate\Store;
use App\Http\Requests\Api\Admin\Delegate\Update;
use App\Http\Resources\Api\Users\UserResource;
use App\Models\Delegate;
use App\Models\User;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DelegateController extends Controller
{
    use ResponseTrait , PaginationTrait;
    public function index(){
        $delegate = User::where('user_type' , 'delegate')->paginate(10);
        $paginated = $this->paginationModel($delegate);
        $response = UserResource::collection($delegate);
        return $this->successData( ['Paginated' => $paginated , $response]);
    }
    public function show($id){
        $delegate = User::where('user_type' , 'delegate')->find($id);
        if (!$delegate)
            return $this->failMsg('هذا المستخدم غير موجود');
        $resource = new UserResource($delegate);
        return $this->successData($resource);
    }
    public function search(Request $request)
    {
        $users = User::where('user_type' , 'delegate')->where('name', 'like', '%' . $request->search . '%')
            ->orWhere('email', 'like', '%' . $request->search . '%')
            ->orWhere('phone', 'like', '%' . $request->search . '%')
            ->paginate(10);
        $resource = UserResource::collection($users);
        $paginated = $this->paginationModel($users);
        return $this->successData( ['Paginated' => $paginated , $resource]);
    }
    public function store(Store $request){
        DB::beginTransaction();
        $exists = User::where('user_type', 'delegate')->where('phone', $request->phone)->count();
        if ($exists) {
            return $this->response('fail', __('auth.invalid-registered'));
        }
        $user = User::create($request->validated() + ['is_approved' => 0, 'user_type' => 'delegate']);

        Delegate::create($request->validated() + ['user_id' => $user->id]);
        DB::commit();
        $user->sendVerificationCode();
        $userData = new UserResource($user->refresh());
        Report::addToLog('بانشاء حساب Delegate جديد');
        return $this->response('success' , 'تم انشاء حساب Delegate جديد', ['user' => $userData]);
    }
    public function update(Update $request, $id){
        $delegate = User::where('user_type' , 'delegate')->find($id);
        if (!$delegate)
            return $this->failMsg('هذا المستخدم غير موجود');
        $delegate->update($request->validated());
          $delegate->delegate()->get()->first()->update($request->validated());
        $resource = new UserResource($delegate->refresh());
        Report::addToLog('بتعديل بيانات Delegate');
        return $this->response('success' , 'تم تعديل بيانات Delegate', ['user' => $resource]);
    }
    public function destroy($id){
        $delegate = User::where('user_type' , 'delegate')->find($id);
        if (!$delegate)
            return $this->failMsg('هذا المستخدم غير موجود');
        $delegate->delete();
        Report::addToLog('بحذف المندوب');
        return $this->response('success' , 'تم حذف المندوب');
    }
    public function destroyAll(Request $request){
        $ids = $request->ids;
        $delegate = User::where('user_type' , 'delegate')->whereIn('id' , $ids)->get();
        if (!$delegate)
            return $this->failMsg('هذا المستخدم غير موجود');
        $delegate->each->delete();
        Report::addToLog('بحذف العديد من  المندوبين');
        return $this->response('success' , 'تم حذف العديد من  المندوبين');
    }
}
