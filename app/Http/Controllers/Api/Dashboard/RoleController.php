<?php

namespace App\Http\Controllers\Api\Dashboard;


use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Role\Create;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    use ResponseTrait;


    public function store(Create $request)
    {
        $role = Role::create($request->validated());
        return $this->response('success', __('auth.registered'), ['role' => $role]);
    }

}
