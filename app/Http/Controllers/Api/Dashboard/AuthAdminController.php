<?php

namespace App\Http\Controllers\Api\Dashboard;


use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Admin\Store;
use App\Http\Requests\Api\Admin\Auth\Login;
use App\Http\Resources\Api\Admin\Admin\AdminResource;
use App\Models\Admin;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthAdminController extends Controller
{
    use ResponseTrait;

    public function login(Login $request)
    {
        $admin = Admin::where('email', $request->email)->first();
        if (!$admin) {
            return $this->response('fail', __('auth.failed'));
        }
        if (!Hash::check($request->password, $admin->password)) {
            return $this->response('fail', __('auth.incorrect_pass'));
        }
        if ($admin->is_blocked) {
            return $this->blockedReturn($admin);
        }
        return $this->response('success', __('auth.Login'), ['user' => $admin->login()]);
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->user()->logout();
        return $this->successMsg(__('auth.Logout'));
    }

    public function Profile(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $requestToken = ltrim($request->header('authorization'), 'Bearer ');
        $adminData = AdminResource::make($admin)->setToken($requestToken);
        return $this->successData(['admin' => $adminData]);
    }
}
