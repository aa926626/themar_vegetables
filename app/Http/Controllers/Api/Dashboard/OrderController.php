<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Orders\Update;
use App\Http\Resources\Api\DashOrders\OrderResource;
use App\Models\Order;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;

class OrderController extends Controller
{
    use ResponseTrait , PaginationTrait ,  Report;
    public function index()
    {
        $orders = Order::with('user', 'delegate', 'userAddress')->get();
        $response = OrderResource::collection($orders);
        return $this->successData(['orders' => $response]);
    }

    public function show($id)
    {
        $order = Order::with('user', 'delegate', 'userAddress')->find($id);
        $response = new OrderResource($order);
        return $this->successData(['order' => $response]);
    }

   public function update(Update $request , $id)
   {
       $order = Order::find($id);
       $order->update($request->validated());
       $response = new OrderResource($order);
       return $this->successData(['order' => $response]);
   }

   public function destroy($id){
         $order = Order::find($id);
         $order->delete();
         return $this->successMsg('تم الحذف بنجاح');
   }
}



