<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\privacyPolicy\Store;
use App\Http\Resources\Api\privacyPolicy\privacyPolicyResource;
use App\Http\Resources\Api\Products\ProductsResource;
use App\Models\privacyPolicy;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class privacyPolicyController extends Controller
{
    use ResponseTrait , Report;

    public  function index(){
        $privacyPolicy = privacyPolicy::paginate(10);
        $response  =  privacyPolicyResource::collection($privacyPolicy);
        return $this->successData($response);
    }

        public function show(privacyPolicy $privacyPolicy){
        $response = new privacyPolicyResource($privacyPolicy);
        return $this->successData($response);
    }

    public function store(Store $request){
        $privacyPolicy = privacyPolicy::create($request->validated());
        $response = new privacyPolicyResource($privacyPolicy);
        return $this->successData($response);
    }

    public function update(Store $request, privacyPolicy $privacyPolicy){
        $privacyPolicy->update($request->validated());
        $response = new privacyPolicyResource($privacyPolicy);
        return $this->successData($response);
    }

    public function destroy(privacyPolicy $privacyPolicy){
        $privacyPolicy->delete();
        return $this->successMsg('Deleted Successfully');
    }

//تواصل معنا
}
