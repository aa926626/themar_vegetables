<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Offers\Store;
use App\Http\Requests\Api\Admin\Offers\Update;
use App\Http\Resources\Api\Offers\OfferResource;
use App\Models\Offers;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    use ResponseTrait , PaginationTrait , Report;

    public function index(Request $request)
    {
        $offers = Offers::paginate(10);
        $pagination = $this->paginationModel($offers);
        $response = OfferResource::collection($offers);
        return $this->successData(['data' => $response , 'pagination' => $pagination]);
    }

    public function show($id)
    {
        $offer = Offers::findOrFail($id);
        $response = new OfferResource($offer);
        return $this->successData(['data' => $response]);
    }

    public function store(Store $request)
    {
        $offer = Offers::create($request->validated());
        $response = new OfferResource($offer);
        Report::addToLog('اضافة عرض جديد');
        return $this->successData(['data' => $response]);
    }

    public function update(Update $request , $id)
    {
        $offer = Offers::findOrFail($id);
        $offer->update($request->validated());
        $response = new OfferResource($offer);
        Report::addToLog('تعديل عرض');
        return $this->successData(['data' => $response]);
    }

    public function destroy($id)
    {
        $offer = Offers::findOrFail($id);
        $offer->delete();
        Report::addToLog('حذف عرض');
        return $this->successMsg('Offer deleted successfully');
    }

}
