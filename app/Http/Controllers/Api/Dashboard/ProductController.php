<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Product\Create;
use App\Http\Requests\Api\Admin\Product\Update;
use App\Http\Resources\Api\Products\HomeProductsResource;
use App\Http\Resources\Api\Products\ProductsCollection;
use App\Http\Resources\Api\Products\ProductsResource;
use App\Traits\Report;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Models\Product;
use App\Traits\ResponseTrait;


class ProductController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $categories = Product::paginate(10);
        $categorey = ProductsCollection::collection($categories);
        return $this->successData(['categories' => $categorey]);
    }
    public function show($id)
    {
        $category = Product::find($id);
        if (!$category)
            return $this->failMsg('هذا المنتج غير موجود');
        $resource = new ProductsResource($category);
        return $this->successData(['category' => $resource]);
    }
    public function search($name)
    {
        $products = Product::where('name', 'like', '%' . $name . '%')->get();
        $products = HomeProductsResource::collection($products);
        if ($products->count() > 0) {
            return $this->successData(['products' => $products]);
        } else {
            return $this->successData(['products' => __('site.no_data')]);
        }
    }
    public function store(Create $request)
    {
        $product = Product::create($request->validated());
        Report   ::addToLog( ' اضافة منتج جديد');
        $resource = new ProductsResource($product);
        return $this->successData(['category' => $resource]);
    }
    public function update(Update $request , $id){
        $product = Product::find($id);
        if (!$product)
            return $this->failMsg('هذا المنتج غير موجود');
        $product->update($request->validated());
        Report   ::addToLog( ' تعديل منتج');
        $resource = new ProductsResource($product);
        return $this->successData(['Product' => $resource]);

    }
    public function removeImage(Request $request , $id){
        $product = Product::find($id);
        $image =  $product->media()->whereId($request->image_id)->first();
        deleteFile( $image->file_name , 'products');
        $image->delete();
        return $this->successMsg('تم حذف الصورة بنجاح');
    }

    public function destroy($id){
        Product  ::find($id)->delete();
        Report   ::addToLog( ' حذف منتج');
        return $this->successMsg('تم حذف المنتج بنجاح');
    }







}

