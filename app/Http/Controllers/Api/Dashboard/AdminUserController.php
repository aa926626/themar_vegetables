<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Users\Store;
use App\Http\Requests\Api\Admin\Users\Update;
use App\Http\Resources\Api\Users\UserResource;
use App\Models\User;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    use ResponseTrait , PaginationTrait;


    public function index()
    {
       $users = User::where('user_type' , 'user')->paginate(10);
       $resource = UserResource::collection($users);
       $paginated = $this->paginationModel($users);
       return $this->successData( ['Paginated' => $paginated , $resource]);
    }

    public function show($id)
    {
        $user = User::find($id);
        if (!$user)
            return $this->failMsg('هذا المستخدم غير موجود');
        $resource = new UserResource($user);
        return $this->successData($resource);
    }

    public function search(Request $request)
    {
        $users = User::where('user_type' , 'user')->where('name', 'like', '%' . $request->search . '%')
                   ->orWhere('email', 'like', '%' . $request->search . '%')
                   ->orWhere('phone', 'like', '%' . $request->search . '%')
                   ->paginate(10);
        $resource = UserResource::collection($users);
        $paginated = $this->paginationModel($users);
        return $this->successData( ['Paginated' => $paginated , $resource]);
    }

    public function store(Store $request)
    {
        $user = User::create($request->validated() + ['country_code' => 966]);
        $resource = new UserResource($user->refresh());
        return $this->response('success' , 'تم انشاء حساب User جديد', ['user' => $resource]);
    }

    public function update(Update $request, $id)
    {
        $user = User::find($id);
        if (!$user)
            return $this->failMsg('هذا المستخدم غير موجود');
        $user->update($request->validated());
        $resource = new UserResource($user->refresh());
        return $this->response('success' , 'تم التعديل  بنجاح' ,$resource);
    }

    public function destroy($id)
    {
        $user = User::find($id)->where('user_type' , 'user')->first();
        if (!$user)
            return $this->failMsg('هذا المستخدم غير موجود');
        $user->delete();
        return $this->successMsg('تم حذف المستخدم بنجاح');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->ids;
        User::whereIn('id', $ids)->delete();
        return $this->successMsg('تم حذف المستخدمين بنجاح');
    }
}
