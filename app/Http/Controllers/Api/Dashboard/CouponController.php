<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Coupons\Store;
use App\Http\Requests\Api\Admin\Coupons\Update;
use App\Http\Resources\Api\Categories\CategoreResource;
use App\Http\Resources\Api\Coupon\CouponResource;
use App\Models\Coupon;
use App\Traits\ResponseTrait;

class CouponController extends Controller
{
    use ResponseTrait;
    public function index()
    {
        $coupons = Coupon::paginate(10);
        $resource = CouponResource::collection($coupons);
        return $this->successData($resource);
    }

    public function show($id)
    {
        $coupon = Coupon::find($id);
        if (!$coupon)
            return $this->failMsg('هذا الكوبون غير موجود');
        $resource = new CouponResource($coupon);
        return $this->successData($resource);
    }

    public function store(Store $request)
    {
        $coupon = Coupon::create($request->validated());
        $resource = new CouponResource($coupon);
        return $this->successData($resource);
    }

    public function update(Update $request, $id)
    {
        $coupon = Coupon::find($id);
        if (!$coupon)
            return $this->failMsg('هذا الكوبون غير موجود');
        $coupon->update($request->validated());
        $resource = new CouponResource($coupon);
        return $this->successData($resource);
    }

    public function destroy($id)
    {
        $coupon = Coupon::find($id);
        if (!$coupon)
            return $this->failMsg('هذا الكوبون غير موجود');
        $coupon->delete();
        return $this->successMsg('تم حذف الكوبون بنجاح');
    }
}
