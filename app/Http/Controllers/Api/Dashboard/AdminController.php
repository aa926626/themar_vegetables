<?php

namespace App\Http\Controllers\Api\Dashboard;


use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Admin\Store;
use App\Http\Requests\Api\Admin\Admin\Update;
use App\Http\Resources\Api\Admin\Admin\AdminResource;
use App\Http\Resources\Api\Notifications\NotificationsResource;
use App\Models\Admin;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use ResponseTrait, PaginationTrait;

    public function index()
    {
        $admins     = Admin::latest()->paginate(30);
        $adminsData = AdminResource::collection($admins);
        $pagination = $this->paginationModel($admins);
        return      $this->successOtherData(['Store' => $admins]);
    }

    public function store(Store $request)
    {
        $admin   = Admin::create($request->validated());
        Report   ::addToLog('  اضافه مدير');
        $data    = new AdminResource($admin);
        return   $this->successOtherData(['Dashboard' => $data]);
    }

    public function update($id, Update $request)
    {
        $admin   = Admin::findOrFail($id);
        $admin   ->update($request->validated());
        Report   ::addToLog('  تعديل مدير');
        $data    = new AdminResource($admin);
        return   $this->successOtherData(['Dashboard' => $data]);
    }

    public function show($id)
    {
        $admin  = Admin::findOrFail($id);
        $data   = new AdminResource($admin);
        return  $this->successOtherData(['Dashboard' => $data]);
    }
    public function destroy($id)
    {
        if (1 == $id) return $this->successMsg('لا يمكن حذف هذا المدير');
        Admin ::findOrFail($id)->delete();
        Report::addToLog('  حذف مدير');
        return $this->successMsg('تم حذف المدير بنجاح ');
    }
    public function destroyAll(Request $request)
    {
        $requestIds = array_column($request->data, 'id');
        Admin ::whereIn('id', $requestIds)->where('id', '!=', 1)->get()->each->delete();
        Report::addToLog('  حذف العديد من المديرين');
        return $this->successMsg('تم حذف المديرين بنجاح ');
    }
    public function search(Request $request)
    {
        $admins = Admin::where('name', 'like', '%' . $request->search . '%')
            ->orWhere('email', 'like', '%' . $request->search . '%')
            ->orWhere('phone', 'like', '%' . $request->search . '%')
            ->paginate(10);
        $adminsData = AdminResource::collection($admins);
        $pagination = $this->paginationModel($admins);
        return $this->successOtherData(['admins' => $adminsData, 'pagination' => $pagination]);
    }

    public function notifications()
    {
       $notification =  auth()->guard('admin')->user()->unreadNotifications->markAsRead();
       if (!$notification) return $this->successMsg('لا يوجد اشعارات جديدة');
        $notifications = NotificationsResource::collection(auth()->user()->notifications()->paginate(20));
        $pagination = $this->paginationModel($notifications);
        return $this->successData(['pagination' => $pagination, 'notifications' => $notifications]);
    }
    public function countUnreadNotifications()
    {
        $count = auth()->guard('admin')->user()->unreadNotifications->count();
        return $this->successData(['count' => $count]);
    }
    public function deleteNotifications(Request $request)
    {
        $requestIds = array_column($request->data, 'id');
        auth()->guard('admin')->user()->notifications()->whereIn('id', $requestIds)->get()->each->delete();
        return $this->successMsg('تم حذف الاشعارات بنجاح ');
    }
}
