<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\Category\Create;
use App\Http\Requests\Api\Admin\Category\Update;
use App\Http\Resources\Api\Categories\CategoreResource;
use App\Http\Resources\Api\Categories\CategoriesCollection;
use App\Http\Resources\Api\Products\ProductsCollection;
use App\Models\Category;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    use ResponseTrait , PaginationTrait;
    public function index(){
        $categories = Category::paginate(10);
        $categorey = CategoriesCollection::collection($categories);
        $pagination = $this->paginationModel($categorey);
        return $this->successData(['categories' => $categorey]);
    }
    public function store(Create $request){
        $category = Category::create($request->validated());
        Report   ::addToLog( ' اضافة قسم جديد');
        $resource = new CategoreResource($category);
        return $this->successData(['category' => $resource]);
    }
    public function update(Update $request, $id){
        $category = Category::find($id);
        $category->update($request->validated());
        Report   ::addToLog( ' تعديل قسم');
        $resource = new CategoreResource($category);
        return $this->successData(['category' => $resource]);
    }
    public function show($id){
        $category = Category::find($id);
        if (!$category)
            return $this->failMsg('هذا القسم غير موجود');
        $resource = new CategoreResource($category);
        return $this->successData(['category' => $resource]);
    }
    public function destroy($id){
        $category = Category::find($id);
        if (!$category)
            return $this->failMsg('هذا القسم غير موجود');
        if ($category->products->count() > 0)
            return $this->failMsg('لا يمكن حذف هذا القسم لانه يحتوي على منتجات');
        $category->delete();
        Report   ::addToLog( ' حذف قسم');
        return $this->successMsg('تم حذف القسم بنجاح');
    }

    public function search($name){
        $Categories = Category::where('name', 'like', '%' . $name . '%')->get();
        $Categories = CategoreResource::collection($Categories);
        if ($Categories->count() > 0) {
            return $this->successData(['products' => $Categories]);
        } else {
            return $this->successData(['products' => __('site.no_data')]);
        }
    }



}
