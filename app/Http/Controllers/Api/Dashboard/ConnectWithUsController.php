<?php

namespace App\Http\Controllers\Api\Dashboard;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\ConnectWithUs\Store;
use App\Http\Requests\Api\Admin\ConnectWithUs\Update;
use App\Http\Resources\Api\ConnectWithUs\ConnectWithUsResource;
use App\Models\ConnectWithUs;
use App\Traits\PaginationTrait;
use App\Traits\Report;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ConnectWithUsController extends Controller
{
    use ResponseTrait , PaginationTrait , Report;

    public function index()
    {
        $data = ConnectWithUs::paginate(10);
        $pag = $this->paginationModel($data);
        $res =  ConnectWithUsResource::collection($data);
        return $this->successData(['data' => $res, 'pag' => $pag]);
    }

    public function show(ConnectWithUs $connectWithUs)
    {
        $res = new ConnectWithUsResource($connectWithUs);
        return $this->successData($res);
    }

    public function store(Store $request)
    {
        $slider = ConnectWithUs::create($request->validated());
        $resource = new ConnectWithUsResource($slider);
        Report::addToLog('اضافة معلومات التواصل  جديدة');
        return $this->successData(['ConnectWithUs' => $resource]);
    }

    public function update(Update $request, ConnectWithUs $connectWithUs)
    {
        $data = $request->validated();
        $connectWithUs->update($data);
        return $this->response('msg', __('apis.updated') ,$connectWithUs);
    }

    public function destroy(ConnectWithUs $connectWithUs)
    {
        $connectWithUs->delete();
        return $this->successMsg(__('apis.deleted'));
    }




}
