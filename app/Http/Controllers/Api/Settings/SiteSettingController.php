<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Admin\ConnectMessage\Store as ConnectMessageStore;
use App\Http\Resources\Api\ConnectWithUs\ConnectWithUsResource;
use App\Http\Resources\Api\FAQs\FAQsResource;
use App\Http\Resources\Api\privacyPolicy\privacyPolicyResource;
use App\Models\ConnectMessage;
use App\Models\ConnectWithUs;
use App\Models\FAQs;
use App\Models\privacyPolicy;
use App\Models\SiteSetting;
use App\Traits\ResponseTrait;

class SiteSettingController extends Controller
{
    use ResponseTrait;

    public function about() {
        $about = SiteSetting::where(['key' => 'about_' . lang()])->first()->value;
        return $this->successData(['about' => $about]);
    }

    public function terms() {
        $terms = SiteSetting::where(['key' => 'terms_' . lang()])->first()->value;
        return $this->successData(['terms' => $terms]);
    }

    public function privacy() {
        $privacy = SiteSetting::where(['key' => 'privacy_' . lang()])->first()->value;
        return $this->successData(['privacy' => $privacy]);
    }

    public function FAQS(){
        $faqs = FAQs::paginate(10);
        $pagination = $this->paginationModel($faqs);
        $response = FAQsResource::collection($faqs);
        return $this->successData(['faqs' => $response, 'pagination' => $pagination]);
    }

    public function PrivacyPolicy(){
        $privacy_policy = PrivacyPolicy::first();
        $response = new PrivacyPolicyResource($privacy_policy);
        return $this->successData(['privacy_policy' => $response]);
    }

    public function ConnectWithUs(){
        $connect_with_us = ConnectWithUs::Active();
        $response = new ConnectWithUsResource($connect_with_us);
        return $this->successData(['connect_with_us' => $response]);
    }

    //ConnectMessage
    public function ConnectMessage(ConnectMessageStore $request)
    {
        ConnectMessage::create($request->validated() + ['user_id' => auth()->user()->id ?? null]);
        return $this->successMsg('تم ارسال الرسالة بنجاح');
    }

}
