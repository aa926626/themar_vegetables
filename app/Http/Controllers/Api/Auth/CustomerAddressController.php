<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Address\StoreRequest;
use App\Http\Resources\Api\Address\UserAddressResource;
use App\Models\UserAddress;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CustomerAddressController extends Controller
{
    use ResponseTrait;
    public function address(Request $request)
    {
        return $this->response('success', 'address', UserAddressResource::collection(UserAddress::UserId()->get()));
    }

    public function storeAddress(StoreRequest $request)
    {
        $address = $request->user()->addresses()->create($request->validated());
        return $this->response('success', 'address', new UserAddressResource($address));
    }

    public function updateAddress(StoreRequest $request, UserAddress $address)
    {
        $address->update($request->validated());
        return $this->response('success', 'address', new UserAddressResource($address));
    }

    public function deleteAddress(UserAddress $address)
    {
        $address->delete();
        return $this->response('success', 'address', new UserAddressResource($address));
    }


}
