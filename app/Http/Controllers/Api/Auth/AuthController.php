<?php

namespace App\Http\Controllers\Api\Auth;

use App;
use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Auth\ActivateRequest;
use App\Http\Requests\Api\Auth\CheckPhoneRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\ResendCodeRequest;
use App\Http\Requests\Api\Auth\SignUpDelegateRequest;
use App\Http\Requests\Api\Auth\SignUpUserRequest;
use App\Http\Requests\Api\Auth\UpdatePasswordRequest;
use App\Http\Requests\Api\Auth\UpdateProfileRequest;
use App\Http\Resources\Api\Users\UserResource;
use App\Models\Delegate;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use ResponseTrait;

    public function signUpUser(SignUpUserRequest $request)
    {
        $user = User::create($request->validated() + ['country_code' => 966]);
        $user->sendVerificationCode();
        $UserReso = new UserResource($user->refresh());
        return $this->response('success', __('auth.registered'), ['user' => $UserReso]);
    }

    public function signUpDelegate(SignUpDelegateRequest $request)
    {

        DB::beginTransaction();
        $exists = User::where('user_type', 'delegate')->where('phone', $request->phone)->count();
        if ($exists) {
            return $this->response('fail', __('auth.invalid-registered'));
        }
        $user = User::create($request->validated() + ['is_approved' => 0, 'user_type' => 'delegate']);

        Delegate::create($request->validated() + ['user_id' => $user->id]);
        DB::commit();

        $user->sendVerificationCode();

        $userData = new UserResource($user->refresh());
        return $this->response('success', __('auth.registered'), ['user' => $userData]);
    }

    public function activate(ActivateRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if ($user->active) {
            return $this->response('fail', __('auth.already-activated'));
        }
        if ($user->code != $request->code) {
            return $this->response('fail', __('auth.invalid-code'));
        }
        return $this->response('success', __('auth.activated'), ['user' => $user->markAsActive()->login()]);
    }

    public function resendCode(ResendCodeRequest $request)
    {
        $user = User::where('phone', $request->phone)
            ->where('country_code', $request->country_code ?? 966)
            ->first();
        if (!$user) {
            return $this->response('fail', __('auth.wrong'));
        }
        if ($user->active) {
            return $this->response('fail', __('auth.already-activated'));
        }
        $user->sendVerificationCode();
        return $this->response('success', __('auth.send_activated'));
    }

    public function login(LoginRequest $request)
    {

        $user = User::where('phone', $request->phone)
            ->where('country_code', $request->country_code ?? 966)
            ->first();
        if (!$user) {
            return $this->response('fail', __('auth.failed'));
        }
        if (!Hash::check($request->password, $user->password)) {
            return $this->response('fail', __('auth.incorrect_pass'));
        }

        if ($user->user_type != $request['user_type']) {
            return $this->failMsg($user->user_type . ' ' . __('auth.not_authorized_user_type'));
        }

        if (!$user->is_approved) {
            return $this->failMsg(__('auth.approved'));
        }

        if ($user->is_blocked) {
            return $this->blockedReturn($user);
        }

        if (!$user->active) {
            return $this->phoneActivationReturn($user);
        }

        return $this->response('success', __('auth.Login'), ['user' => $user->login()]);
    }

    public function logout()
    {
        Auth::guard('api')->user()->logout();
        return $this->successMsg(__('auth.Logout'));
    }

    public function getProfile(Request $request)
    {
        $user = Auth::guard('api')->user();
        $requestToken = ltrim($request->header('authorization'), 'Bearer ');
        $userData = UserResource::make($user)->setToken($requestToken);
        return $this->successData(['user' => $userData]);
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = auth()->user();
        $user->update($request->validated());
        $requestToken = ltrim($request->header('authorization'), 'Bearer ');
        $userData = UserResource::make($user->refresh())->setToken($requestToken);
        return $this->response('success', __('auth.update'), ['user' => $userData]);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = auth()->user();
        $user->update($request->validated());
        return $this->successMsg(__('auth.update'));
    }

    public function resetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if ($user->code != $request->code) {
            return $this->response('fail', __('auth.code_invalid'));
        }
        $user->update(['password' => $request->password, 'code' => null, 'code_expire' => null]);
        return $this->successMsg(__('auth.password_changed'));
    }

    public function CheckPhone(CheckPhoneRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();
        if (!$user) {
            return $this->response('fail', __('auth.incorrect_key_or_phone'));
        }
        $user->sendVerificationCode();
        return $this->response('success', __('auth.code_re_send'));
    }

    public function changeLang(Request $request)
    {
        $user = auth()->user();
        $lang = in_array($request->lang, languages()) ? $request->lang : 'ar';
        $user->update(['lang' => $lang]);
        App::setLocale($lang);
        return $this->successMsg(__('auth.update_lang'));
    }





}

