<?php

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Api\Review\Store;
use App\Http\Resources\Api\Categories\CategoriesCollection;
use App\Http\Resources\Api\Products\HomeProductsResource;
use App\Http\Resources\Api\Products\ProductsResource;
use App\Http\Resources\Api\Slider\SliderResource;
use App\Models\Category;
use App\Models\Favorite;
use App\Models\Product;
use App\Models\ReviewRating;
use App\Models\Slider;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;

class HomeController extends Controller
{
    use ResponseTrait , PaginationTrait;
    public function index(){
            $sliders  =   SliderResource::collection(Slider::where('status', 1)->get());
            $categorey = CategoriesCollection::collection(Category::Active()->paginate(6));
             $products = Product::OrderBy('id', 'desc')->Active()->HasQuantity()->ActiveCategory()->take(4)->get();
             $pro = HomeProductsResource::collection($products);
             $data = ['sliders' => $sliders, 'categories' => $categorey, 'products' => $pro,];
             return $this->successData($data);
    }

    public function showProduct($id)
    {
        $product = Product::find($id);
        if (!$product) {return $this->failMsg(__('apis.no_data'));}
        $related_products = Product::where('category_id', $product->category_id)->where('id', '!=', $id)->Active()->HasQuantity()->ActiveCategory()->take(4)->get();
        $related_products = HomeProductsResource::collection($related_products);
        $product = new ProductsResource($product);
        return $this->successData(['product' => $product, 'related_products' => $related_products ]);
    }

    public function SearchProducts($name)
    {
        $products = Product::where('name', 'like', '%' . $name . '%')->get();
        $products = HomeProductsResource::collection($products);
        if ($products->count() > 0) {
            return $this->successData(['products' => $products]);
        } else {
            return $this->successData(['products' => __('site.no_data')]);
        }
    }

    public function AddAndDeleteFavorite($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return $this->failMsg(__('apis.no_data'));
        }
        $favorite = Favorite::where('product_id', $id)->where('user_id', auth()->user()->id)->first();
        if ($favorite) {
            $favorite->delete();
            return $this->successMsg(__('apis.removed_from_favorite'));
        } else {
            Favorite::create([
                'product_id' => $id,
                'user_id' => auth()->user()->id,
            ]);
            return $this->successMsg(__('apis.added_to_favorite'));
        }
    }

    public function ShowFavorites()
    {
        $favorites = Favorite::where('user_id', auth()->user()->id)->get();
        $favorites = HomeProductsResource::collection($favorites->pluck('product'));
        return $this->successData(['favorites' => $favorites]);
    }

    public function AddReview(Store $request, $id)
    {
        $product = Product::find($id);
        if (!$product) {
            return $this->failMsg(__('apis.no_data'));
        }
        if (ReviewRating::where('product_id', $id)->where('user_id', auth()->user()->id)->first()) {
            return $this->failMsg(__('apis.review_added_before'));
        }
        ReviewRating::create([
            'product_id' => $id,
            'user_id' => auth()->user()->id,
            'rating' => $request->rating,
            'review' => $request->review,
        ]);
        return $this->successMsg(__('apis.review_added'));

    }




}



