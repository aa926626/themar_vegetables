<?php

use App\Http\Controllers\Api\Dashboard\SettingController;
use App\Http\Controllers\Api\Logic\Delegate\OrderDelegateController;
use App\Http\Controllers\Api\Dashboard\ConnectMessageController;
use App\Http\Controllers\Api\Dashboard\privacyPolicyController;
use App\Http\Controllers\Api\Dashboard\ConnectWithUsController;
use App\Http\Controllers\Api\Dashboard\LogActivityController;
use App\Http\Controllers\Api\Auth\CustomerAddressController;
use App\Http\Controllers\Api\Logic\Users\UserCartController;
use App\Http\Controllers\Api\Settings\SiteSettingController;
use App\Http\Controllers\Api\Dashboard\AdminUserController;
use App\Http\Controllers\Api\Dashboard\AuthAdminController;
use App\Http\Controllers\Api\Dashboard\DelegateController;
use App\Http\Controllers\Api\Dashboard\CategoryController;
use App\Http\Controllers\Api\Dashboard\ProductController;
use App\Http\Controllers\Api\Dashboard\CountryController;
use App\Http\Controllers\Api\Logic\Users\UserController;
use App\Http\Controllers\Api\Dashboard\CouponController;
use App\Http\Controllers\Api\Dashboard\SliderController;
use App\Http\Controllers\Api\Dashboard\OrderController;
use App\Http\Controllers\Api\Dashboard\AdminController;
use App\Http\Controllers\Api\Dashboard\OfferController;
use App\Http\Controllers\Api\Dashboard\CityController;
use App\Http\Controllers\Api\Dashboard\FAQSController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Home\HomeController;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api', 'middleware' => ['api-lang']], function () {
    /***************************** AuthController Start *****************************/

    Route::post('sign-up-user',         [AuthController::class, 'signUpUser']);
    Route::post('sign-up-delegate',     [AuthController::class, 'signUpDelegate']);
    Route::post('activate',             [AuthController::class, 'activate']);
    Route::post('resend-code',          [AuthController::class, 'resendCode']);
    Route::post('sign-in',              [AuthController::class, 'login']);
    Route::delete('sign-out',           [AuthController::class, 'logout']);
    Route::post('check-phone',          [AuthController::class, 'checkPhone']);
    Route::post('reset-password',       [AuthController::class, 'resetPassword']);

    Route::group(['middleware' => ['auth:api', 'is-active']], function () {

        Route::get('profile',                    [AuthController::class, 'getProfile']);
        Route::post('update-profile',            [AuthController::class, 'updateProfile']);
        Route::post('update-password',           [AuthController::class, 'updatePassword']);
        Route::get('address',                    [CustomerAddressController::class, 'address']);
        Route::post('store-address',             [CustomerAddressController::class, 'storeAddress']);
        Route::post('update-address/{address}',  [CustomerAddressController::class, 'updateAddress']);


    });
    /***************************** AuthController end *****************************/





    /***************************** Home Start *****************************/
    Route::get('home',                       [HomeController::class, 'index']);
    Route::get('product/{id}',               [HomeController::class, 'showProduct']);
    Route::get('search-products/{name}',     [HomeController::class, 'SearchProducts']);
    Route::post('product/favorite/{id}',     [HomeController::class, 'AddAndDeleteFavorite'])->middleware('auth:api');
    Route::get('product/favorites/show',     [HomeController::class, 'ShowFavorites'])->middleware('auth:api');
    Route::post('products/review/{id}',      [HomeController::class, 'AddReview'])->middleware('auth:api');
    /***************************** Home  end *****************************/

    /***************************** SiteSetting Start *****************************/
    Route::get('about-us',                   [SiteSettingController::class, 'about']);
    Route::get('terms',                      [SiteSettingController::class, 'terms']);
    Route::get('privacy',                    [SiteSettingController::class, 'privacy']);
    Route::get('FAQS',                       [SiteSettingController::class, 'FAQS']);
    Route::get('PrivacyPolicy',              [SiteSettingController::class, 'PrivacyPolicy']);
    Route::get('ConnectWithUs',              [SiteSettingController::class, 'ConnectWithUs']);
    Route::post('ConnectMessage',            [SiteSettingController::class, 'ConnectMessage']);
    /***************************** SiteSetting End *****************************/






    Route::group(['middleware' => ['OptionalJwtMiddleware' ,'auth:api' ]], function () {
        /***************************** Cart Start *****************************/
        Route::post('cart/store',                  [UserCartController::class, 'store']);
        Route::post('cart/add/products',           [UserCartController::class, 'addProducts']);
        Route::get('cart/show',                    [UserCartController::class, 'CartItemShow']);
        Route::post('cart/update',                 [UserCartController::class, 'update']);
        Route::post('cart/delete',                 [UserCartController::class, 'destroy']);
        Route::post('cart/delete-all',             [UserCartController::class, 'destroyAll']);
        Route::get('cart/count',                   [UserCartController::class, 'count']);
        Route::post('cart/coupon',                 [UserCartController::class, 'discountCoupon']);
        /***************************** Cart  end *****************************/
    });




    Route::group(['middleware' => ['auth:api', 'is-active']], function () {

        /***************************** Order Start *****************************/
        Route::post('order/store-order',            [UserController::class, 'storeOrder']);
        Route::get('order/show-order',              [UserController::class, 'showOrder']);
        Route::post('order/delete-order',           [UserController::class, 'deleteOrder']);
        Route::get('order/user-pending-orders',     [UserController::class, 'userPendingOrders']);
        Route::get('order/user-finished-orders',    [UserController::class, 'userFinishedOrders']);
        Route::get('order/user-order-details/{id}', [UserController::class, 'userOrderDetails']);

        /***************************** Order  end *****************************/




        /***************************** Notifications Start *****************************/

        Route::get('notifications',                 [UserController::class, 'Notifications']);
        Route::get('count-notifications',           [UserController::class, 'countUnreadNotifications']);
        Route::post('delete-notification/{id}',     [UserController::class, 'deleteNotification']);
        Route::post('delete-notifications',         [UserController::class, 'deleteNotifications']);

        /***************************** Notifications end *****************************/




        /***************************** OrderDelegate Start *****************************/
        Route::post('delegate-main-orders',          [OrderDelegateController::class, 'DelegateMainOrders']);
        Route::post('delegate-pending-orders',       [OrderDelegateController::class, 'DelegatePendingOrders']);
        Route::post('delegate-finished-orders',      [OrderDelegateController::class, 'DelegateFinishedOrders']);
        Route::post('delegate-order-details',        [OrderDelegateController::class, 'DelegateOrderDetails']);
        Route::post('delegate-delivery-start',       [OrderDelegateController::class, 'DelegateDeliveryStart']);
        Route::post('delegate-delivery-end',         [OrderDelegateController::class, 'DelegateDeliveryEnd']);
        Route::post('delegate-search-orders',        [OrderDelegateController::class, 'DelegateSearchOrders']);
        Route::post('delegate-show-order',           [OrderDelegateController::class, 'ShowOrderDelegate']);

        /***************************** OrderDelegate end *****************************/


    });









    /***************************** Dashboard Start *****************************/
    Route::group(['prefix' => 'admin', 'namespace'=> 'Dashboard', 'as' => 'admin.'], function () {

        /***************************** Auth Dashboard Start *****************************/
        Route::post('login',          [AuthAdminController::class, 'login']);
        Route::post('logout',         [AuthAdminController::class, 'logout'])->middleware('auth:admin');
        Route::get ('profile-admin',  [AuthAdminController::class, 'profile'])->middleware('auth:admin');

        /***************************** Auth Dashboard End *****************************/

        Route::group(['middleware' => ['Dashboard' , 'role:super-admin']], function () {

            /***************************** Dashboard Start *****************************/
            Route::get('admins',                     [AdminController::class, 'index']);
            Route::post('admins/store',              [AdminController::class, 'store']);
            Route::post('admins/update/{id}',        [AdminController::class, 'update']);
            Route::get('admins/{id}',                [AdminController::class, 'show']);
            Route::post('admins/delete/{id}',        [AdminController::class, 'destroy']);
            Route::post('admins/delete-all',         [AdminController::class, 'destroyAll']);
            Route::get('notifications/admin',        [AdminController::class, 'Notifications']);
            Route::get('count-notifications/admin',  [AdminController::class, 'countUnreadNotifications']);
            Route::post('delete-notifications/admin',[AdminController::class, 'deleteNotifications']);
            Route::get('search/admins',              [AdminController::class, 'search']);
            /***************************** Dashboard End *****************************/

            /***************************** User Start *****************************/
            Route::get('users',                      [AdminUserController::class, 'index']);
            Route::get('users/{id}',                 [AdminUserController::class, 'show']);
            Route::post('users/store',               [AdminUserController::class, 'store']);
            Route::post('users/update/{id}',         [AdminUserController::class, 'update']);
            Route::post('users/delete/{id}',         [AdminUserController::class, 'destroy']);
            Route::post('users/delete-all',          [AdminUserController::class, 'destroyAll']);
            Route::post('search/users',              [AdminUserController::class, 'search']);
            /***************************** User end *****************************/

            /***************************** Delegates Start *****************************/
            Route::get('delegates',                  [DelegateController::class, 'index']);
            Route::get('delegates/{id}',             [DelegateController::class, 'show']);
            Route::post('search/delegates',          [DelegateController::class, 'search']);
            Route::post('delegates/store',           [DelegateController::class, 'store']);
            Route::post('delegates/update/{id}',     [DelegateController::class, 'update']);
            Route::post('delegates/delete/{id}',     [DelegateController::class, 'destroy']);
            Route::post('delegates/delete-all',      [DelegateController::class, 'destroyAll']);
            /***************************** Delegates End *****************************/


           /***************************** Categorise Start *****************************/
            Route::get('categories',                 [CategoryController::class, 'index']);
            Route::post('categories/store',          [CategoryController::class, 'store']);
            Route::post('categories/update/{id}',    [CategoryController::class, 'update']);
            Route::get('category/{id}',              [CategoryController::class, 'show']);
            Route::post('categories/delete/{id}',    [CategoryController::class, 'destroy']);
            Route::get('search/categories/{name}',   [CategoryController::class, 'search']);
          /***************************** Categories End *******************************/


            /***************************** Products Start *****************************/
            Route::get('products',                    [ProductController::class, 'index']);
            Route::get('product/{id}',                [ProductController::class, 'show']);
            Route::get('search/products/{name}',      [ProductController::class, 'search']);
            Route::post('products/store',             [ProductController::class, 'store']);
            Route::post('products/update/{id}',       [ProductController::class, 'update']);
            Route::post('products/remove-image/{id}', [ProductController::class, 'removeImage']);
            Route::post('products/delete/{id}',       [ProductController::class, 'destroy']);
            /***************************** Products  end *****************************/


            /***************************** Sliders Start *****************************/
            Route::get('sliders',                     [SliderController::class, 'index']);
            Route::get('slider/{id}',                 [SliderController::class, 'show']);
            Route::post('sliders/store',              [SliderController::class, 'store']);
            Route::post('sliders/update/{id}',        [SliderController::class, 'update']);
            Route::post('sliders/delete/{id}',        [SliderController::class, 'destroy']);
            /***************************** Sliders  end *****************************/


            /***************************** Coupons  Start *****************************/
            Route::get('coupons',                     [CouponController::class, 'index']);
            Route::get('coupon/{id}',                 [CouponController::class, 'show']);
            Route::post('coupons/store',              [CouponController::class, 'store']);
            Route::post('coupons/update/{id}',        [CouponController::class, 'update']);
            Route::post('coupons/delete/{id}',        [CouponController::class, 'destroy']);
            /***************************** Coupons  end *****************************/

            /***************************** Offers  Start *****************************/
            Route::get('offers',                         [OfferController::class, 'index']);
            Route::get('offer/{id}',                     [OfferController::class, 'show']);
            Route::post('offers/store',                  [OfferController::class, 'store']);
            Route::post('offers/update/{id}',            [OfferController::class, 'update']);
            Route::post('offers/delete/{id}',            [OfferController::class, 'destroy']);
            /***************************** Offers  end *****************************/

            /***************************** Orders  Start *****************************/
            Route::get('orders',                         [OrderController::class, 'index']);
            Route::get('order/{id}',                     [OrderController::class, 'show']);
            Route::post('orders/update/{id}',            [OrderController::class, 'update']);
            Route::post('orders/delete/{id}',            [OrderController::class, 'destroy']);
            /***************************** Orders  end *****************************/


            /***************************** Countries  Start *****************************/
            Route::get('countries',                          [CountryController::class, 'index']);
            Route::get('country/{country}',                  [CountryController::class, 'show']);
            Route::post('countries/store',                   [CountryController::class, 'store']);
            Route::post('countries/update/{country}',        [CountryController::class, 'update']);
            Route::post('countries/delete/{country}',        [CountryController::class, 'destroy']);
            /***************************** Countries  End *****************************/


            /***************************** Cities  Start *****************************/
            Route::get('cities',                          [CityController::class, 'index']);
            Route::get('city/{city}',                     [CityController::class, 'show']);
            Route::post('cities/store',                   [CityController::class, 'store']);
            Route::post('cities/update/{city}',           [CityController::class, 'update']);
            Route::post('cities/delete/{city}',           [CityController::class, 'destroy']);
            /***************************** Cities  End *****************************/


            /***************************** FAQS  Start *****************************/
            Route::get('faqs',                          [FAQSController::class, 'index']);
            Route::get('faq/{faq}',                     [FAQSController::class, 'show']);
            Route::post('faqs/store',                   [FAQSController::class, 'store']);
            Route::post('faqs/update/{faq}',            [FAQSController::class, 'update']);
            Route::post('faqs/delete/{faq}',            [FAQSController::class, 'destroy']);
            /***************************** FAQS  End *****************************/



            /***************************** PrivacyPolicy  Start *****************************/
            Route::get('privacy-policy',                          [PrivacyPolicyController::class, 'index']);
            Route::get('privacy-policy/{privacy_policy}',         [PrivacyPolicyController::class, 'show']);
            Route::post('privacy-policy/store',                   [PrivacyPolicyController::class, 'store']);
            Route::post('privacy-policy/update/{privacy_policy}', [PrivacyPolicyController::class, 'update']);
            Route::post('privacy-policy/delete/{privacy_policy}', [PrivacyPolicyController::class, 'destroy']);
            /***************************** PrivacyPolicy  End *****************************/



            /***************************** ConnectWithUs  Start *****************************/
            Route::get('connect-with-us',                           [ConnectWithUsController::class, 'index']);
            Route::get('connect-with-us/{connectWithUs}',           [ConnectWithUsController::class, 'show']);
            Route::post('connect-with-us/store',                    [ConnectWithUsController::class, 'store']);
            Route::post('connect-with-us/update/{connectWithUs}',   [ConnectWithUsController::class, 'update']);
            Route::post('connect-with-us/delete/{connectWithUs}',   [ConnectWithUsController::class, 'destroy']);
            /***************************** ConnectWithUs  End ********************************/
            /***************************** connectMessage  Start *****************************/
            Route::get('connect-messages',                          [ConnectMessageController::class, 'index']);
            Route::get('connect-message/{connectMessage}',          [ConnectMessageController::class, 'show']);
            Route::post('connect-messages/delete/{connectMessage}', [ConnectMessageController::class, 'destroy']);
            Route::post('connect-messages/delete-all',              [ConnectMessageController::class, 'destroyAll']);
            /***************************** connectMessage  End *****************************/





            /***************************** log_activities  Start *****************************/
            Route::get('log_activities',                          [LogActivityController::class, 'index']);
            Route::get('log_activity/{log_activity}',             [LogActivityController::class, 'show']);
            Route::post('log_activities/delete/{log_activity}',   [LogActivityController::class, 'destroy']);
            Route::post('log_activities/delete-all',              [LogActivityController::class, 'destroyAll']);
            /***************************** log_activities  End *****************************/

        });


    });







    /***************************** Dashboard End *****************************/
























});


