<?php

namespace Database\Seeders;

use App\Models\Offers;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class OfferProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
$faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            DB::table('offer_products')->insert([
                'offer_id' => $faker->numberBetween(1, 2),
                'product_id' => $faker->numberBetween(1, 20),
            ]);
        }

//        DB::table('offer_products')->insert([
//            'offer_id' => 1,
//            'product_id' => 1,
//        ]);


    }
}

