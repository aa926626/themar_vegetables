<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);


        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(ProductsImagesSeeder::class);
        $this->call(CouponSeeder::class);
        $this->call(OffersSeeder::class);
        $this->call(OfferProductsSeeder::class);
        $this->call(FAQsSeeder::class);
        $this->call(PrivacyPolicySeeder::class);
        $this->call(ConnectWithUsSeeder::class);
        $this->call(ConnectMessageSeeder::class);
    }
}
