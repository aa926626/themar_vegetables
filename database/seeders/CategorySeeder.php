<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'   => json_encode(['ar' => 'الخضار', 'en' => 'vegetables'], JSON_UNESCAPED_UNICODE),
                'image'  => '1.jpg',
                'status' => true,

            ], [
                'name'   => json_encode(['ar' => 'البهرات', 'en' => 'spices'], JSON_UNESCAPED_UNICODE),
                'image'  => '2.jpg',
                'status' => true,

            ], [
                'name'   => json_encode(['ar' => 'التمور', 'en' => 'dates'], JSON_UNESCAPED_UNICODE),
                'image'  => '3.jpg',
                'status' => true,
            ], [
                'name'   => json_encode(['ar' => 'المكسرات', 'en' => 'Nuts'], JSON_UNESCAPED_UNICODE),
                'image'  => '4.jpg',
                'status' => true,
            ], [
                'name'   => json_encode(['ar' => 'اللحوم', 'en' => 'meat'], JSON_UNESCAPED_UNICODE),
                'image'  => '5.jpg',
                'status' => true,
            ], [
                'name'   => json_encode(['ar' => 'الفواكه', 'en' => 'fruits'], JSON_UNESCAPED_UNICODE),
                'image'  => '6.jpg',
                'status' => true,
            ], [
                'name'   => json_encode(['ar' => 'الاسماك', 'en' => 'fish'], JSON_UNESCAPED_UNICODE),
                'image'  => '7.jpg',
                'status' => true,
            ],
        ]);

    }
}
