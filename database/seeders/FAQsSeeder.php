<?php

namespace Database\Seeders;

use App\Models\FAQs;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class FAQsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('f_a_qs')->insert([
          [
              'question' => json_encode(['en' => 'How to use the website?', 'ar' => 'كيفية استخدام الموقع؟'] , JSON_UNESCAPED_UNICODE),
              'answer' => json_encode(['en' => 'You can use the website by following the instructions in the website.', 'ar' => 'يمكنك استخدام الموقع بمتابعة التعليمات في الموقع.'], JSON_UNESCAPED_UNICODE)
              ],
            [
                'question' => json_encode(['en' => 'How to use the website?', 'ar' => 'كيفية استخدام الموقع؟'] , JSON_UNESCAPED_UNICODE),
                'answer' => json_encode(['en' => 'You can use the website by following the instructions in the website.', 'ar' => 'يمكنك استخدام الموقع بمتابعة التعليمات في الموقع.'], JSON_UNESCAPED_UNICODE)
            ],
            [
                'question' => json_encode(['en' => 'How to use the website?', 'ar' => 'كيفية استخدام الموقع؟'] , JSON_UNESCAPED_UNICODE),
                'answer' => json_encode(['en' => 'You can use the website by following the instructions in the website.', 'ar' => 'يمكنك استخدام الموقع بمتابعة التعليمات في الموقع.'], JSON_UNESCAPED_UNICODE)
            ]

        ]);
    }
}
