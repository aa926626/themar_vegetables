<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
Use DB;

class ConnectMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('connect_messages')->insert([
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرحبا بكم في موقعنا',
                'user_id' => 108,
            ],
            [
                'name' => 'محمد',
                'phone' => '0599999999',
                'message' => 'مرح بكم في موقعنا',
                'user_id' => 108,
            ],

        ]);
    }
}
