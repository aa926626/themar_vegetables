<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory;
Use DB;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['name' => json_encode(['ar' => 'طماطم', 'en' => 'Tomatoes'], JSON_UNESCAPED_UNICODE), 'slug' => 'Tomatoes', 'description' => json_encode(['ar' => 'الطماطم مصدر جيد لفيتامين سي', 'en' => 'Tomatoes are a good source of vitamin C'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '1', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'خيار', 'en' => 'choice'], JSON_UNESCAPED_UNICODE), 'slug' => 'choice', 'description' => json_encode(['ar' => 'خيار مصدر جيد لفيتامين سي', 'en' => 'choice are a good source of vitamin C'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '1', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'جزر', 'en' => 'carrot'], JSON_UNESCAPED_UNICODE), 'slug' => 'carrot', 'description' => json_encode(['ar' => 'جزر مصدر جيد لفيتامين سي', 'en' => 'carrot are a good source of vitamin C'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '1', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'الفلفل', 'en' => 'Pepper'], JSON_UNESCAPED_UNICODE), 'slug' => 'Pepper', 'description' => json_encode(['ar' => 'الفلفل مصدر جيد لفيتامين سي', 'en' => 'Pepper are a good source of vitamin C'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '1', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'فلفل اسود', 'en' => 'black pepper'], JSON_UNESCAPED_UNICODE), 'slug' => 'black pepper', 'description' => json_encode(['ar' => 'الفلفل الاسود يستخدم في  طهي  الطعام', 'en' => 'black Pepper It is used to cook food'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '2', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'حبهان', 'en' => 'Cardamom'], JSON_UNESCAPED_UNICODE), 'slug' => 'Cardamom', 'description' => json_encode(['ar' => 'حبهان يستخدم في  طهي  الطعام', 'en' => 'Cardamom It is used to cook food'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '2', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'قرفه', 'en' => 'Cinnamon'], JSON_UNESCAPED_UNICODE), 'slug' => 'Cinnamon', 'description' => json_encode(['ar' => 'قرفه يستخدم في  طهي  الطعام', 'en' => 'Cinnamon It is used to cook food'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '2', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'ينسون', 'en' => 'Anise'], JSON_UNESCAPED_UNICODE), 'slug' => 'Anise', 'description' => json_encode(['ar' => 'ينسون يستخدم في  طهي  الطعام', 'en' => 'Anise It is used to cook food'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '2', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'اللوز المكرمل', 'en' => 'Caramelized almonds'], JSON_UNESCAPED_UNICODE), 'slug' => 'Caramelized-almonds', 'description' => json_encode(['ar' => 'تساعد هذه التمور على خفض خطر الإصابة بالأمراض القلبية', 'en' => 'These dates help reduce the risk of heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '3', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'اللوز المحمص', 'en' => 'roasted almonds'], JSON_UNESCAPED_UNICODE), 'slug' => 'roasted-almonds', 'description' => json_encode(['ar' => 'تساعد هذه التمور على خفض خطر الإصابة بالأمراض القلبية', 'en' => 'These dates help reduce the risk of heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '3', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'قشور الليمون المسكرة', 'en' => 'Candied lemon peels'], JSON_UNESCAPED_UNICODE), 'slug' => 'Candied-lemon-peels', 'description' => json_encode(['ar' => 'تساعد هذه التمور على خفض خطر الإصابة بالأمراض القلبية', 'en' => 'These dates help reduce the risk of heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '3', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'تمور الخضري', 'en' => 'Khudri dates'], JSON_UNESCAPED_UNICODE), 'slug' => 'Khudri-dates', 'description' => json_encode(['ar' => 'تساعد هذه التمور على خفض خطر الإصابة بالأمراض القلبية', 'en' => 'These dates help reduce the risk of heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '3', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'اللوز', 'en' => 'Almonds'], JSON_UNESCAPED_UNICODE), 'slug' => 'Almonds', 'description' => json_encode(['ar' => 'تحسين صحة الشرايين تقليل الالتهابات المرتبطة بأمراض القلب ', 'en' => 'Improve arterial health Reducing inflammation associated with heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '4', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'البندق', 'en' => 'Nut'], JSON_UNESCAPED_UNICODE), 'slug' => 'Nut', 'description' => json_encode(['ar' => 'تحسين صحة الشرايين تقليل الالتهابات المرتبطة بأمراض القلب ', 'en' => 'Improve arterial health Reducing inflammation associated with heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '4', 'featured' => '1', 'status' => true],
            ['name' => json_encode(['ar' => 'الفستق', 'en' => 'pistachio'], JSON_UNESCAPED_UNICODE), 'slug' => 'pistachio', 'description' => json_encode(['ar' => 'تحسين صحة الشرايين تقليل الالتهابات المرتبطة بأمراض القلب ', 'en' => 'Improve arterial health Reducing inflammation associated with heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '4', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'الكاجو', 'en' => 'cashews'], JSON_UNESCAPED_UNICODE), 'slug' => 'cashews', 'description' => json_encode(['ar' => 'تحسين صحة الشرايين تقليل الالتهابات المرتبطة بأمراض القلب ', 'en' => 'Improve arterial health Reducing inflammation associated with heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '4', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'المكاديميا', 'en' => 'macadamia'], JSON_UNESCAPED_UNICODE), 'slug' => 'macadamia', 'description' => json_encode(['ar' => 'تحسين صحة الشرايين تقليل الالتهابات المرتبطة بأمراض القلب ', 'en' => 'Improve arterial health Reducing inflammation associated with heart disease'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '4', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'الكتف', 'en' => 'the shoulder'], JSON_UNESCAPED_UNICODE), 'slug' => 'the-shoulder', 'description' => json_encode(['ar' => 'غني بالكالسيوم وصديق للعظام مصدر مفيد للبروتين قد يقلل من خطر الإصابة بمرض السكري من النوع  تعزز الإحساس بالشبع ', 'en' => 'Rich in calcium, friendly to the bones, a beneficial source of protein, may reduce the risk of developing type 2 diabetes, and promote a sense of youth'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '5', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'الريش', 'en' => 'feathers'], JSON_UNESCAPED_UNICODE), 'slug' => 'feathers', 'description' => json_encode(['ar' => 'غني بالكالسيوم وصديق للعظام مصدر مفيد للبروتين قد يقلل من خطر الإصابة بمرض السكري من النوع  تعزز الإحساس بالشبع ', 'en' => 'Rich in calcium, friendly to the bones, a beneficial source of protein, may reduce the risk of developing type 2 diabetes, and promote a sense of youth'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '5', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'لحم الصدر', 'en' => 'brisket'], JSON_UNESCAPED_UNICODE), 'slug' => 'brisket', 'description' => json_encode(['ar' => 'غني بالكالسيوم وصديق للعظام مصدر مفيد للبروتين قد يقلل من خطر الإصابة بمرض السكري من النوع  تعزز الإحساس بالشبع ', 'en' => 'Rich in calcium, friendly to the bones, a beneficial source of protein, may reduce the risk of developing type 2 diabetes, and promote a sense of youth'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '5', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'لحمة الرأس', 'en' => 'head weft'], JSON_UNESCAPED_UNICODE), 'slug' => 'head-weft', 'description' => json_encode(['ar' => 'غني بالكالسيوم وصديق للعظام مصدر مفيد للبروتين قد يقلل من خطر الإصابة بمرض السكري من النوع  تعزز الإحساس بالشبع ', 'en' => 'Rich in calcium, friendly to the bones, a beneficial source of protein, may reduce the risk of developing type 2 diabetes, and promote a sense of youth'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '5', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'موز', 'en' => 'Banana'], JSON_UNESCAPED_UNICODE), 'slug' => 'Banana', 'description' => json_encode(['ar' => 'الفاكهة مليئة بالفيتامينات والمعادن والألياف ومضادات الأكسدة ', 'en' => 'Fruit is full of vitamins, minerals, fiber and antioxidants'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '6', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'عنب', 'en' => 'Grapes'], JSON_UNESCAPED_UNICODE), 'slug' => 'Grapes','description' => json_encode(['ar' => 'الفاكهة مليئة بالفيتامينات والمعادن والألياف ومضادات الأكسدة ', 'en' => 'Fruit is full of vitamins, minerals, fiber and antioxidants'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '6', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'بِطّيخ', 'en' => 'Watermelon'], JSON_UNESCAPED_UNICODE), 'slug' => 'Watermelon', 'description' => json_encode(['ar' => 'الفاكهة مليئة بالفيتامينات والمعادن والألياف ومضادات الأكسدة ', 'en' => 'Fruit is full of vitamins, minerals, fiber and antioxidants'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '6', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'تفاح', 'en' => 'Apple'], JSON_UNESCAPED_UNICODE), 'slug' => 'Apple', 'description' => json_encode(['ar' => 'الفاكهة مليئة بالفيتامينات والمعادن والألياف ومضادات الأكسدة ', 'en' => 'Fruit is full of vitamins, minerals, fiber and antioxidants'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '6', 'featured' => '1', 'status' => true,],


            ['name' => json_encode(['ar' => 'البوري', 'en' => 'mullet'], JSON_UNESCAPED_UNICODE), 'slug' => 'mullet', 'description' => json_encode(['ar' => ' تحسين جودة النوم. تقليل خطر الإصابة بمرض السكري ', 'en' => 'Improve sleep quality. Reducing the risk of diabetes'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '7', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'القاروص', 'en' => 'seabass'], JSON_UNESCAPED_UNICODE), 'slug' => 'seabass', 'description' => json_encode(['ar' => ' تحسين جودة النوم. تقليل خطر الإصابة بمرض السكري ', 'en' => 'Improve sleep quality. Reducing the risk of diabetes'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '7', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'الدنيس', 'en' => 'bream'], JSON_UNESCAPED_UNICODE), 'slug' => 'bream', 'description' => json_encode(['ar' => ' تحسين جودة النوم. تقليل خطر الإصابة بمرض السكري ', 'en' => 'Improve sleep quality. Reducing the risk of diabetes'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '7', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'البلطي', 'en' => 'tilapia'], JSON_UNESCAPED_UNICODE), 'slug' => 'tilapia', 'description' => json_encode(['ar' => ' تحسين جودة النوم. تقليل خطر الإصابة بمرض السكري ', 'en' => 'Improve sleep quality. Reducing the risk of diabetes'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '7', 'featured' => '1', 'status' => true,],
            ['name' => json_encode(['ar' => 'التونة', 'en' => 'Tuna'], JSON_UNESCAPED_UNICODE), 'slug' => 'Tuna', 'description' => json_encode(['ar' => ' تحسين جودة النوم. تقليل خطر الإصابة بمرض السكري ', 'en' => 'Improve sleep quality. Reducing the risk of diabetes'], JSON_UNESCAPED_UNICODE), 'price' => '10', 'quantity' => '5', 'category_id' => '7', 'featured' => '1', 'status' => true,],
        ]);

    }
}


