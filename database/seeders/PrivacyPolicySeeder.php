<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PrivacyPolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('privacy_policies')->insert([
           ['title' => json_encode(['en' => 'Privacy Policy', 'ar' => 'سياسة الخصوصية'] , JSON_UNESCAPED_UNICODE),
            'description' => json_encode(['en' => 'Privacy Policy', 'ar' => 'سياسة الخصوصية'] , JSON_UNESCAPED_UNICODE)
           ],

        ]);

    }
}
