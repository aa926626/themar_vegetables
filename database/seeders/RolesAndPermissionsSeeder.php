<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use App\Models\Admin;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $addProduct       =     Permission::create(['name' => 'add product' , 'guard_name' => 'admin']);
        $editProduct      =     Permission::create(['name' => 'edit product', 'guard_name' => 'admin']);
        $deleteProduct    =     Permission::create(['name' => 'delete product', 'guard_name' => 'admin']);
        $viewProduct      =     Permission::create(['name' => 'view product', 'guard_name' => 'admin']);

        $addCategory      =     Permission::create(['name' => 'add category', 'guard_name' => 'admin']);
        $editCategory     =     Permission::create(['name' => 'edit category', 'guard_name' => 'admin']);
        $deleteCategory   =     Permission::create(['name' => 'delete category', 'guard_name' => 'admin']);
        $viewCategory     =     Permission::create(['name' => 'view category', 'guard_name' => 'admin']);

        $addAdmin         =     Permission::create(['name' => 'add admin', 'guard_name' => 'admin']);
        $editAdmin        =     Permission::create(['name' => 'edit admin', 'guard_name' => 'admin']);
        $deleteAdmin      =     Permission::create(['name' => 'delete admin', 'guard_name' => 'admin']);
        $viewAdmin        =     Permission::create(['name' => 'view admin', 'guard_name' => 'admin']);


        # Role
        Role::create(['name' => 'super-admin' ,  'guard_name' => 'admin'])->givePermissionTo(Permission::all());
        Role::create(['name' => 'system-admin'  , 'guard_name' => 'admin'])->givePermissionTo([
            $addProduct, $editProduct, $deleteProduct, $viewProduct,
            $addCategory, $editCategory, $deleteCategory, $viewCategory
        ]);
        Role::create(['name' => 'store-owner' ,  'guard_name' => 'admin'])->givePermissionTo([
            $addProduct, $editProduct, $deleteProduct, $viewProduct,
        ]);
        Role::create(['name' => 'customer' , 'guard_name' => 'admin'])->givePermissionTo([
            $viewProduct, $viewCategory
        ]);

        Admin::find(1)->assignRole('super-admin');
        Admin::find(2)->assignRole('system-admin');
        Admin::find(3)->assignRole('store-owner');
        Admin::find(4)->assignRole('customer');





    }
}
