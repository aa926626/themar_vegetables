<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;


class ProductsImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images[] = ['file_name' => '1.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '2.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '3.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '4.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '5.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '6.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '7.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];
        $images[] = ['file_name' => '8.jpg', 'file_type' => 'image/jpg', 'file_size' => rand(100, 900),'file_path' => 'products', 'file_status' => true, 'file_sort' => 0];

        Product::all()->each(function ($product) use ($images) {
            $product->media()->createMany(Arr::random($images, rand(2, 3)));
        });
    }
}
