<?php

namespace Database\Seeders;

use App\Models\Offers;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offers = [
            [
                'type' => 'Products',
                'max_use' => 100,
                'image' => 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
                'use_times' => 0,
                'user_times' => 0,
                'amount' => 2,
                'max_discount' => 10,
                'amount_type' => 'ratio',
                'can_use_with_coupon' => false,
                'product_use' => 1,
                'start_date' => '2024-01-01 00:00:00',
                'expire_date' => '2024-12-31 00:00:00',
                'status' => true,
            ],
            [
                'type' => 'Categories',
                'max_use' => 100,
                'image' => 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png',
                'use_times' => 0,
                'user_times' => 0,
                'amount' => 100,
                'max_discount' => 100,
                'amount_type' => 'ratio',
                'can_use_with_coupon' => false,
                'product_use' => 0,
                'start_date' => '2021-01-01 00:00:00',
                'expire_date' => '2021-12-31 00:00:00',
                'status' => true,
            ],
        ];

        foreach ($offers as $offer) {
            $offer = Offers::create($offer);
        }
    }
}
