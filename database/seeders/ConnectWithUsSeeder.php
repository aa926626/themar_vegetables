<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
Use DB;

class ConnectWithUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('connect_with_us')->insert([
            'lat' => '30.0444',
            'lng' => '31.2357',
            'map_desc' => 'مصر',
            'phone' => '01016736771',
            'email' => 'yousef.elshrqawe@gmail.com',
            'active' => 1,
            'created_at' => now()->format('Y-m-d'),
            'updated_at' => now()->format('Y-m-d'),
        ]);
    }
}
