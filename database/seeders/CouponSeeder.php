<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        DB::table('coupons')->insert([
            'coupon_num' => '1Null1',
            'type' => 'number',
            'discount' => '0',
            'max_discount' => '0',
            'expire_date' => '2039-01-01',
            'max_use' => '900000',
            'use_times' => '0',
            'status' => 'available',
        ]);
        DB::table('coupons')->insert([
            'coupon_num' => '123456',
            'type' => 'number',
            'discount' => '3',
            'max_discount' => '2',
            'expire_date' => '2024-01-01',
            'max_use' => '5',
            'use_times' => '0',
            'status' => 'available',
        ]);
    }
}
