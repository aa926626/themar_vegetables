<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string            ('name')->index();
            $table->string            ('country_code', 5)->default('966');
            $table->string            ('phone', 15)->unique()->index();
            $table->string            ('code', 10)->nullable();
            $table->timestamp         ('code_expire')->nullable();
            $table->string            ('lang', 2)->default('ar');
            $table->unsignedBigInteger('city_id')->nullable();
            $table->decimal           ('lat', 10, 8)->nullable();
            $table->decimal           ('lng', 10, 8)->nullable();
            $table->string            ('map_desc', 50)->nullable();
            $table->string            ('id_number', 50)->unique();
            $table->string            ('image', 50)->default('default.png');
            $table->boolean           ('active')->default(0);
            $table->boolean           ('is_blocked')->default(0);
            $table->boolean           ('is_approved')->default(1);
            $table->enum              ('user_type', ['user', 'delegate'])->default('user');
            $table->string            ('email')->unique();
            $table->string            ('password');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
