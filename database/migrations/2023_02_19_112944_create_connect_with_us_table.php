<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connect_with_us', function (Blueprint $table) {
            $table->id();
            $table->decimal           ('lat', 10, 8)->nullable();
            $table->decimal           ('lng', 10, 8)->nullable();
            $table->string            ('map_desc', 50)->nullable();
            $table->string            ('phone', 15)->index();
            $table->string            ('email', 50);
            $table->boolean           ('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connect_with_us');
    }
};
//رسالة
