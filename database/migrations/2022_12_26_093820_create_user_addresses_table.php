<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId ('user_id')->constrained()->cascadeOnDelete();
            $table->enum      ('address_type', [ 'home', 'work'])->default('home');
            $table->string    ('city', 50)->nullable();
            $table->decimal   ('lat', 10, 8)->nullable();
            $table->decimal   ('lng', 10, 8)->nullable();
            $table->string    ('map_desc', 50)->nullable();
            $table->string    ('phone', 15);
            $table->string    ('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
};
