<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {

            $table->id();
            $table->enum                 ('type', ['Products', 'Categories']);
            $table->integer              ('max_use');
            $table->string               ('image')->nullable();
            $table->integer              ('use_times')->default(0);
            $table->integer              ('user_times')->nullable();
            $table->unsignedBigInteger   ('amount')->unsigned();
            $table->double               ('max_discount')->nullable();
            $table->enum                 ('amount_type', ['ratio', 'number']);
            $table->boolean              ('can_use_with_coupon')->default(false);
            $table->integer              ('product_use')->default(0);
            $table->dateTime             ('start_date')->nullable();
            $table->dateTime             ('expire_date')->nullable();
            $table->boolean              ('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
};
