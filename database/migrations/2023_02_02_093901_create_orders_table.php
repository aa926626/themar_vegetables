<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string               ('order_num', 50); //! create with new order dynamic
            $table->integer              ('type')->default(0); //! model const

            $table->foreignId            ('user_id')->constrained()->onDelete('cascade');
            $table->foreignId            ('delegate_id')->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId            ('user_address_id')->nullable()->constrained('user_addresses')->onDelete('cascade');

            $table->double               ('total_products', 9, 2)->default(0);
            $table->double               ('deliver_price', 9, 2)->default(0);
            $table->string               ('discount_code')->nullable();
            $table->double               ('discount' , 9, 2)->default(0);
            $table->double               ('vat_amount', 9, 2)->default(0);
            $table->double               ('total', 9, 2)->default(0);

            $table->integer              ('status')->default(0); //! model const
            $table->enum                 ('preparation_status', ['pending', 'done' , 'canceled'])->default('pending'); //! model const

            $table->integer              ('pay_type')->default(0); //! model const
            $table->integer              ('pay_status')->default(0); //! model const

            $table->decimal              ('lat')->nullable();
            $table->decimal              ('lng')->nullable();
            $table->string               ('map_desc', 255)->nullable();
            $table->date                 ('date')->nullable();
            $table->time                 ('time')->nullable();
            $table->text                 ('notes')->nullable();

            $table->timestamp            ('created_at')->useCurrent();
            $table->timestamp            ('updated_at')->useCurrent()->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
