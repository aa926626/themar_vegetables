<?php

use App\Models\Admin;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password');
            $table->string('avatar', 50)->nullable();
            $table->boolean('is_blocked')->default(0);
            $table->boolean('is_notify')->default(true);

            $table->softDeletes();
            $table->timestamps();
        });

        Admin::create([
            'name' => 'Manager',
            'email' => 'Manager@gManager.com',
            'phone' => '01016736771',
            'password' => 123456789,
        ]);
        Admin::create([
            'name' => 'System',
            'email' => 'System@System.com',
            'phone' => '01016736772',
            'password' => 123456789,
        ]);
        Admin::create([
            'name' => 'store-owner',
            'email' => 'store@store.com',
            'phone' => '01016736773',
            'password' => 123456789,
        ]);
        Admin::create([
            'name' => 'customer',
            'email' => 'customer@customer.com',
            'phone' => '01016736774',
            'password' => 123456789,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
};
