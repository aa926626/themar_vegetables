<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
   */

  'loggedOut'      => 'تم تسجيل الخروج بنجاح',
  'AdminNotify'    => 'اشعار اداري',
  'photoadadded'   => 'تم اضافه الاعلان المصور بنجاح',
  'passwordReset'  => 'تم تحديث كلمه السر بنجاح',
  'newComment'     => 'تعليق جديد ',
  'adDeleted'      => 'تم حذف الاعلان بنجاح ',
  'reportAdded'    => 'تم اضافه البلاغ بنجاح ',
    'signed'     => 'تم تسجيل الدخول بنجاح ',
  'added'          => 'تمت الاضافه بنجاح ',
  'commentAdded'   => 'تمت اضافه التعليق بنجاح ',
  'unauthorize'    => 'غير مسموح بالعمليه ',
  'rated'          => 'تم اضافه تقييمك بنجاح',
  'commentDeleted' => 'تم حذف التعليق بنجاح ',
  'notCompleted'   => 'الطلب غير منتهي',
  'fav'            => 'تم الاضافه الي المفضله بنجاح ',
  'unFav'          => 'تم الحذف من المفضله بنجاح ',
  'openNotify'     => 'تم  تشغيل الاشعارات  بنجاح ',
  'closeNotify'    => 'تم  غلق الاشعارات  بنجاح ',
  'transfered'     => 'تم ارسال الحواله بنجاح ',
  'addAdded'       => 'تم اضافه الاعلان بنجاح ',
  'newMessage'     => 'لديك رساله جديده من :attr',
  'messageSended'  => 'تم ارسال الرساله بنجاح',
  'updated'        => 'تم التعديل بنجاح',
  'refreshed'      => 'تم تحديث الاعلان بنجاح',
  'phone_changed'  => 'تم تغيير رقم الهاتف بنجاح',
  'email_changed'  => 'تم تغيير الايميل بنجاح',
  'edit_profile'   => 'تعديل بياناتي',
  'send_activated' => 'تم ارسال كود التفعيل بنجاح',
  'complaint_send' => 'تم ارسال الشكوي بنجاح',
  'data_incorrect' => 'البيانات المدخله غير صحيحة',
  'success'        => 'تم الارسال بنجاح',
  'deleted'        => 'تم الحذف بنجاح',
  'confirmed'      => 'تم التأكيد بنجاح',
  'send'           => 'ارسال فقط',
  'send_receive'        => 'ارسال واستلام',
  'another_address'     => 'ارسال واستلام عنوان اخر',
  'not_avilable_coupon' => 'الكوبون غير متاح للاستخدام حاليا',
  'coupon_end_at'       => 'الكوبون منتهي الصلاحية في تاريخ :date',
  'disc_amount'         => 'قيمة الخصم ',
  'rs'                  => 'ريال',
  'km'                  => 'كم',
  'unknown'             => 'غير محدد',
  'max_usa_coupon'       => 'تم الوصول للحد الاقصي لاستخدام الكوبون',
  'invalid_order_status' => 'حالة الطلب غير ممكن تحديثها',
    'no_data' => 'لا يوجد بيانات',
    'removed_from_favorite' => 'تم حذف المنتج من المفضله',
    'added_to_favorite' => 'تم اضافه المنتج الي المفضله',
    'review_added' => 'تم اضافه التقييم بنجاح',
    'review_added_before' => 'لقد قمت بتقييم هذا المنتج من قبل',
    'order_already_accepted' => 'تم قبول الطلب من قبل',
    'Enabled' => 'مفعل',
    'Disabled' => 'غير مفعل',
    'Featured' => 'مميز',
    'Not-Featured' => 'غير مميز',
    'blocked' => 'محظور',
    'unblocked' => 'غير محظور',
    'approved' => 'موافق عليه',
    'not_approved' => 'غير موافق عليه',
    'created' => 'تم الانشاء بنجاح',
];
