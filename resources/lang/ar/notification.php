<?php

return [

    'TestNotification'   => 'اختبار الاشعارات',
    'title_admin_notify' => 'اشعار اداري ',
    'title_finish_order' => 'انهاء طلب',
    'body_finish_order'  => 'تم الانتهاء من المنتج رقم :order_num',
    'NEW_ORDER' => [
        'title' => ' طلب جديد',
        'body' => ' لديك طلب جديد برقم :id',
    ],
    'PAY_ORDER' => [
        'title' => 'تأكيد الدفع',
        'body' => ' قام العميل بتأكيد دفع قيمة الطلب رقم :id',
    ],
    'DELETE_ORDER' => [
        'title' => 'الغاء الطلب',
        'body' => ' قام العميل بالغاء الطلب رقم :id',
    ],
    'FINISH_ORDER' => [
        'title' => 'استلام الطلب',
        'body' => ' قام العميل بتأكيد استلام الطلب رقم :id',
    ],
    'RATE_ORDER' => [
        'title' => 'تقييم الطلب',
        'body' => ' قام العميل بتقييم الطلب رقم :id',
    ],
    'PROVIDER_ACCEPT_ORDER' => [
        'title' => 'قبول الطلب',
        'body' => ' قام مقدم الخدمة بقبول الطلب رقم :id',
    ],
    'PROVIDER_REFUSED_ORDER' => [
        'title' => 'تعذر  تنفيذ الطلب',
        'body' => ' قام مقدم الخدمة بالاعتذار عن تنفيذ الطلب رقم :id',
    ],
    'RECEIVED_DELEGATE' => [
        'title' => '  بدأ توصيل الطلب',
        'body' => ' قام مقدم الخدمة بتسليم للمندوب الطلب رقم :id',
    ],
    'PROVIDER_FINISHED_ORDER' => [
        'title' => '  اصبح الطلب جاهز',
        'body' => ' قام مقدم الخدمة بتأكيد جاهزية الطلب رقم :id',
    ],
    'DELEGATE_ACCEPTED' => [
        'title' => 'وافق المندوب',
        'body' => ' قام  المندوب بالموافقة علي توصيل الطلب رقم :id',
    ],
    'DELEGATE_CANCELLED' => [
        'title' => 'اعتذر',
        'body' => ' قام  المندوب بالموافقة علي توصيل الطلب رقم :id',
    ],
    
];
