<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
   */

  'failed'                   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
  'throttle'                 => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
  'sms'                      => ' كود التفعيل  ',
  'registered'               => 'تم التسجيل بنجاح قم بادخال كود التفعيل  ',
  'code_expired'             => 'كود التفعيل منتهي  ',
  'code_invalid'             => 'كود التفعيل غير صحيح  ',
  'code_re_send'             => 'تم ارسال كود التفعيل ',
  'invalid-registered'       => 'هذا الحساب مسجل مسبقا',
  'invalid_token'            => 'غير مصرح لهذا المستخدم بالدخول jwt',
  'expired_token'            => 'التصريح منتهي jwt',
  'incorrect_pass_or_phone'  => 'كلمة المرور غير صحيحة',
  'not_authorized'           => 'لا تمتلك هذه الصلاحية',
  'not_active'               => ' هذا الحساب غير مفعل تم ارسال كود التفعيل',
  'incorrect_pass'           => 'كلمة المرور غير صحيحة',
  'incorrect_old_pass'       => 'كلمة المرور القديمة غير صحيحة',
  'incorrect_key_or_phone'   => 'تأكد من صحه البريد الالكتروني او الهاتف  ',
  'activated'                => 'تم تفعيل حسابك بنجاح ',
  'already-activated'        => 'تم تفعيل الحساب مسبقا',
    'invalid-code'           => 'كود التفعيل غير صحيح',
  'password_changed'         => 'تم تغيير كلمة السر بنجاح ',
  'blocked'                  => 'تم حظر حسابك من قبل الاداره ',
  'approved'                 => 'هذا الحساب قيد مراجعة الادارة',
  'send_activated'           => 'تم ارسال كود التفعيل بنجاح',
  'unauthenticated'          => 'يرجى اعادة تسجيل الدخول',
  'user'                     => 'المستخدم',
  'provider'                 => 'مقدم الخدمه',
  'delegate'                 => 'المندوب',
  'not_authorized_user_type' => 'هذا الحساب مسجل بتطبيق - ',
  'login_again'              => 'يرجى اعادة تسجيل الدخول',
  'wrong'                    => 'يوجد خطا في  البيانات',
    'Login'                 => 'تم تسجيل الدخول بنجاح',
    'Logout'                 => 'تم تسجيل الخروج بنجاح',
    'update'                 => 'تم التعديل بنجاح',
    'update_lang'            => 'تم تغيير اللغة بنجاح',
    'banned'                 => 'محظور',
    'not_banned'             => 'غير محظور',




];
