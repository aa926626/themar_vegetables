<?php

return [
  'types_0'       => 'بواسطة العميل',
  'types_1'       => 'بواسطة مقدم الخدمة',

   #USER
  'status_0'     => 'فى الانتظار',
  'status_1'     => 'ملغى',
  'status_13'    => 'منتهى',

   #PROVIDER
  'status_2'     => 'تم الموافقة',
  'status_3'     => 'مرفوض',
  'status_4'     => 'تم الاستلام من لمندوب',
  'status_5'     => 'تم الانتهاء',

   #DELEGADE
  'status_6'     => 'تمت موافقه المندوب',
  'status_7'     => 'مرفوض من المندوب',
  'status_8'     => 'تم الاستلام من العميل',
  'status_9'     => 'جار التحضير',
  'status_10'    => 'تم التحضير',
  'status_11'    => 'تم الاستلام من المتجر',
  'status_12'    => 'تم التوصيل للعميل',

  'pay_type_0'   => 'غير محدد',
  'pay_type_1'   => 'نقدا',
  'pay_type_2'   => 'المحفظة',
  'pay_type_3'   => 'البنك',
  'pay_type_4'   => 'اون لاين',

  'pay_status_0' => 'لم يتم',
  'pay_status_2' => 'تم السداد',
  'pay_status_3' => 'تم الارجاع',

  'preparation_pending' => 'جاري التحضير',
  'preparation_done' => 'تم التحضير',
  'preparation_canceled' => 'تم الالغاء',

];
