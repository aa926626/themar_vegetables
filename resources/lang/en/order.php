<?php

return [
   'type_0'       => 'By User',
   'type_1'       => 'By Provider',

    #USER
   'status_0'     => 'Waiting',
   'status_1'     => 'Canceled',
   'status_13'    => 'Completed',

    #PROVIDER
   'status_2'     => 'Accepted',
   'status_3'     => 'Refused',
   'status_4'     => 'Received From Delegate',
   'status_5'     => 'Finished',

    #DELEGADE
   'status_6'     => 'Delegate Accepted',
   'status_7'     => 'Delegate Refused',
   'status_8'     => 'Received From The Customer',
   'status_9'     => 'Preparing',
   'status_10'    => 'Prepared',
   'status_11'    => 'Received From The Laundry',
   'status_12'    => 'Delivered To Customer',

   'pay_type_0'   => 'Undefined',
   'pay_type_1'   => 'Cash',
   'pay_type_2'   => 'Wallet',
   'pay_type_3'   => 'Bank',
   'pay_type_4'   => 'Online',
   'pay_status_0' => 'Pending',
   'pay_status_1' => 'Downpayment',
   'pay_status_2' => 'Done',
   'pay_status_3' => 'Returned',



    'preparation_pending' => 'preparation pending',
    'preparation_done' => 'preparation done',
    'preparation_canceled' => 'preparation canceled',
];
