<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
   */

  'failed'                  => 'These credentials do not match our records.',
  'throttle'                => 'Too many login attempts. Please try again in :seconds seconds.',
  'sms'                     => 'Activation code',
  'registered'              => 'Profile created successfully',
  'activated'               => 'account activated successfully',
    'already-activated'     => 'account already activated',
    'invalid-code'          => 'invalid activation code',
  'code_expired'            => 'Code expired',
  'code_invalid'            => 'Code invalid',
  'code_re_send'            => 'Code resend',
  'invalid-registered'      => 'This account already registered',
  'invalid_token'           => 'jwt token invalid',
  'expired_token'           => 'jwt token expired',
  'not_authorized'          => 'You are not authorized',
  'incorrect_pass_or_phone' => 'Incorrect password',
  'not_active'              => 'Account needs activation',
  'incorrect_pass'          => 'Incorrect password',
  'incorrect_old_pass'      => 'Incorrect Old password',
  'incorrect_key_or_phone'  => 'Check the phone number or email',
  'unauthenticated'         => 'Please login again',
  'approved'                => 'This account is under management review',
  'blocked'                 => 'blocked',
  'user'                    => 'user',
  'provider'                => 'provider',
  'delegate'                => 'delegate',
  'not_authorized_user_type' => 'This account is registered with an application - ',
    'login_again' => 'Please login again',
    'Login'                 => 'Logged in successfully',
    'Logout'                 => 'Signed out successfully',
    'update'                 => 'update successfully',
    'update_lang'                 => 'The language has been changed successfully',
    'banned'                 => 'banned',
    'not_banned'             => 'not banned',





];
