<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
   */

  'loggedOut'           => 'Log Out successfully',
  'AdminNotify'         => 'Dashboard Notify',
  'photoadadded'        => 'Photo Ad Added successfully',
  'passwordReset'       => 'Password Reset successfully',
  'newComment'          => 'New Comment',
  'notCompleted'        => 'The order has not Completed',
  'adDeleted'           => 'Ad Deleted successfully',
  'reportAdded'         => 'Report Added successfully',
  'signed'              => 'sign in successfully',
  'added'               => 'Added successfully',
  'commentAdded'        => 'Comment Added successfully',
  'commentDeleted'      => 'Comment deleted successfully',
  'unauthorize'         => 'un authorize to process this action',
  'rated'               => 'Your Rate Added successfully',
  'fav'                 => 'Added to Favorite successfully',
  'unFav'               => 'Deleted from Favorite successfully',
  'openNotify'          => 'Notifications Open successfully',
  'closeNotify'         => 'Notifications Closed successfully',
  'transfered'          => 'Bank transfer sent successfully',
  'addAdded'            => 'ad Added successfully',
  'newMessage'          => 'You have new message from: attr',
  'messageSended'       => 'Message Sended successfully',
  'updated'             => 'updated successfully',
  'refreshed'           => 'Ad Refreshed successfully',
  'send_activated'      => 'Activation Code Send successfully',
  'phone_changed'       => 'Phone Changed successfully',
  'email_changed'       => 'Email Changed successfully',
  'data_incorrect'      => 'The data entered is incorrect',
  'success'             => 'success',
  'deleted'             => 'deleted',
  'send'                => 'send only',
  'km'                  => 'KM',
  'send_receive'        => 'send and receive',
  'another_address'     => 'Send and receive another address',
  'not_avilable_coupon' => 'The coupon is not available for use at the moment',
   'coupon_end_at'      => 'coupon expired on  : date',
   'disc_amount'        => 'discount amount',
   'rs'                 => 'Saudi Riyals',
   'unknown'            => 'Unknown',
   'invalid_order_status' => 'invalid order status',
    'no_data' => 'No Data',
    'removed_from_favorite' => 'Removed From Favorite',
    'added_to_favorite' => 'Added To Favorite',
    'review_added' => 'Review Added',
    'review_added_before' => 'Review Added Before',
    'order_already_accepted' => 'Order Already Accepted',
    'Enabled' => 'Enabled',
    'Disabled' => 'Disabled',
    'Featured' => 'Featured',
    'Not-Featured' => 'Not Featured',
    'blocked' => 'Blocked',
    'unblocked' => 'Unblocked',
    'approved' => 'Approved',
    'not_approved' => 'Not Approved',
    'created' => 'Created',

];
