<?php

return [
    'TestNotification'   => 'Test Notification Message En',
    'title_finish_order' => 'Awamer',
    'body_finish_order'  => 'Your order has been finished :order_num',
    'title_admin_notify' => 'Administrative notice',
    'NEW_ORDER' => [
        'title' => 'new request',
        'body' => 'You have a new request with :id',
    ],
    'PAY_ORDER' => [
        'title' => 'Payment Confirmation',
        'body' => 'The customer has confirmed the payment of the order number :id',
    ],
    'DELETE_ORDER' => [
        'title' => 'Cancel Request',
        'body' => 'Customer canceled request number :id',
    ],
    'FINISH_ORDER' => [
        'title' => 'receipt of the request',
        'body' => 'Customer has confirmed receipt of order number :id',
    ],
    'RATE_ORDER' => [
        'title' => 'Request evaluation',
        'body' => 'Customer evaluated order number :id',
    ],
    'PROVIDER_ACCEPT_ORDER' => [
        'title' => 'request accepted',
        'body' => 'The service provider accepted the request number :id',
    ],
    'PROVIDER_REFUSED_ORDER' => [
        'title' => 'The request could not be executed',
        'body' => 'The service provider has apologized for not executing the request number :id',
    ],
    'RECEIVED_DELEGATE' => [
        'title' => 'delivery has begun',
        'body' => 'The service provider delivered to the representative the order number :id',
    ],
    'PROVIDER_FINISHED_ORDER' => [
        'title' => 'The request is ready',
        'body' => 'The service provider has confirmed the readiness of the request No. :id',
    ],
    'DELEGATE_ACCEPTED' => [
        'title' => 'The delegate agreed',
        'body' => 'The representative has agreed to deliver the order No. :id',
    ],
    'DELEGATE_CANCELLED' => [
        'title' => 'I apologize',
        'body' => 'The representative has agreed to deliver the order No. :id',
    ],
];
